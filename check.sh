#!/usr/bin/env bash
package="$(basename "$PWD")"
set -e
set -x
time cargo check "--package=$package" --all-targets --all-features
time cargo build "--package=$package" --all-targets --all-features
time cargo fmt "--package=$package"
time cargo clippy "--package=$package" --all-targets --all-features --allow-dirty --allow-staged --fix -- -D clippy::pedantic
time cargo test "--package=$package" --all-targets --all-features
time cargo test "--package=$package" --doc --all-features
../check-readme.sh
time cargo publish "--package=$package" --dry-run "$@"
echo "$0 finished"
