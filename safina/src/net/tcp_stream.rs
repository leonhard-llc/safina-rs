use std::io::{ErrorKind, Read, Write};
use std::net::ToSocketAddrs;

use super::sleep;

pub struct TcpStream {
    std_stream: std::net::TcpStream,
}

impl TcpStream {
    /// Wraps `std_stream` so we can perform async operations on it.
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::set_nonblocking`].
    pub fn new(std_stream: std::net::TcpStream) -> Result<Self, std::io::Error> {
        std_stream.set_nonblocking(true)?;
        crate::timer::start_timer_thread();
        Ok(Self { std_stream })
    }

    /// Opens a TCP connection to `addr`.
    ///
    /// Uses [`crate::executor::schedule_blocking`] to perform the blocking connect call.
    /// Panics if the caller is not running on an [`crate::executor::Executor`].
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::connect`].
    pub async fn connect<A: ToSocketAddrs + Send + 'static>(
        addr: A,
    ) -> Result<Self, std::io::Error> {
        crate::executor::schedule_blocking(move || {
            TcpStream::new(std::net::TcpStream::connect(addr)?)
        })
        .async_recv()
        .await
        .map_err(|_| std::io::Error::new(ErrorKind::Other, "connect thread panicked"))?
    }

    #[must_use]
    pub fn inner(&self) -> &std::net::TcpStream {
        &self.std_stream
    }

    pub fn inner_mut(&mut self) -> &mut std::net::TcpStream {
        &mut self.std_stream
    }

    #[must_use]
    pub fn into_inner(self) -> std::net::TcpStream {
        self.std_stream
    }

    /// Reads some bytes from the socket and places them in `buf`.
    /// Returns the number of bytes read.
    ///
    /// This is an async version of [`std::io::Read::read`].
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::read`].
    pub async fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        loop {
            match self.std_stream.read(buf) {
                Ok(num_read) => return Ok(num_read),
                Err(e) if e.kind() == ErrorKind::WouldBlock || e.kind() == ErrorKind::TimedOut => {
                    sleep().await;
                }
                Err(e) => return Err(e),
            }
        }
    }

    /// Reads all bytes until the socket is shutdown for reading.
    /// Appends the bytes to `buf`.
    ///
    /// This is an async version of [`std::io::Read::read_to_end`].
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::read`].
    pub async fn read_to_end(&mut self, buf: &mut Vec<u8>) -> Result<usize, std::io::Error> {
        let mut chunk = [0_u8; 128 * 1024];
        let mut total_read = 0_usize;
        loop {
            match self.std_stream.read(&mut chunk) {
                Ok(0) => return Ok(total_read),
                Ok(num_read) => {
                    buf.extend_from_slice(&chunk[..num_read]);
                    total_read += num_read;
                }
                Err(e) if e.kind() == ErrorKind::WouldBlock || e.kind() == ErrorKind::TimedOut => {
                    sleep().await;
                }
                Err(e) if e.kind() == ErrorKind::Interrupted => {}
                Err(e) => return Err(e),
            }
        }
    }

    /// Reads all bytes until the socket is shutdown for reading.
    /// Interprets the bytes as a single UTF-8 string and appends it to `buf`.
    ///
    /// This is an async version of [`std::io::Read::read_to_string`].
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::read`].
    ///
    /// Returns [`std::io::ErrorKind::InvalidData`] if the bytes are not valid UTF-8.
    pub async fn read_to_string(&mut self, buf: &mut String) -> Result<usize, std::io::Error> {
        let mut bytes = Vec::new();
        Box::pin(self.read_to_end(&mut bytes)).await?;
        let num_read = bytes.len();
        let mut result = String::from_utf8(bytes)
            .map_err(|e| std::io::Error::new(ErrorKind::InvalidData, format!("{e}")))?;
        core::mem::swap(buf, &mut result);
        Ok(num_read)
    }

    /// Reads the exact number of bytes required to fill `buf`.
    ///
    /// This is an async version of [`std::io::Read::read_exact`].
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::read`].
    pub async fn read_exact(&mut self, buf: &mut [u8]) -> Result<(), std::io::Error> {
        let mut dest = buf;
        while !dest.is_empty() {
            match self.std_stream.read(dest) {
                Ok(0) => return Err(std::io::Error::new(ErrorKind::UnexpectedEof, "eof")),
                Ok(num_read) => {
                    dest = &mut dest[num_read..];
                }
                Err(e) if e.kind() == ErrorKind::WouldBlock || e.kind() == ErrorKind::TimedOut => {
                    sleep().await;
                }
                Err(e) if e.kind() == ErrorKind::Interrupted => {}
                Err(e) => return Err(e),
            }
        }
        Ok(())
    }

    /// Reads bytes into `bufs`, filling each buffer in order.
    /// The final buffer written to may be partially filled.
    ///
    /// Returns the total number of bytes read.
    ///
    /// This is an async version of [`std::net::TcpStream::read_vectored`].
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::read_vectored`].
    pub async fn read_vectored(
        &mut self,
        bufs: &mut [std::io::IoSliceMut<'_>],
    ) -> Result<usize, std::io::Error> {
        loop {
            match self.std_stream.read_vectored(bufs) {
                Ok(num_read) => return Ok(num_read),
                Err(e) if e.kind() == ErrorKind::WouldBlock || e.kind() == ErrorKind::TimedOut => {
                    sleep().await;
                }
                Err(e) => return Err(e),
            }
        }
    }

    /// Waits to receive some data on the socket, then copies it into `buf`.
    ///
    /// Returns the number of bytes copied.
    ///
    /// Repeated calls return the same data.
    /// Call [`TcpStream::read`] to remove data from the socket.
    ///
    /// This is an async version of [`std::net::TcpStream::peek`].
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::peek`].
    pub async fn peek(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        loop {
            match self.std_stream.peek(buf) {
                Ok(num_read) => return Ok(num_read),
                Err(e) if e.kind() == ErrorKind::WouldBlock || e.kind() == ErrorKind::TimedOut => {
                    sleep().await;
                }
                Err(e) => return Err(e),
            }
        }
    }

    /// Writes the bytes in `buf` to the socket.
    /// Returns the number of bytes written.
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::write`].
    pub async fn write(&mut self, buf: &[u8]) -> Result<usize, std::io::Error> {
        loop {
            match self.std_stream.write(buf) {
                Ok(num) => return Ok(num),
                Err(e)
                    if e.kind() == ErrorKind::WouldBlock
                        || e.kind() == ErrorKind::TimedOut
                        || (
                            // http://erickt.github.io/blog/2014/11/19/adventures-in-debugging-a-potential-osx-kernel-bug/
                            // Os { code: 41, kind: Other, message: "Protocol wrong type for socket" }
                            e.kind() == ErrorKind::Other && e.raw_os_error() == Some(41)
                        ) =>
                {
                    sleep().await;
                }
                Err(e) => return Err(e),
            }
        }
    }

    /// Sends all buffered data that was previously written on this socket and
    /// waits for receipt confirmation by the remote machine.
    ///
    /// # Errors
    /// Returns [`std::io::Error`]
    /// if the connection failed
    /// or the remote machine did not respond within a timeout period.
    pub async fn flush(&mut self) -> Result<(), std::io::Error> {
        loop {
            match self.std_stream.flush() {
                Ok(()) => return Ok(()),
                Err(e)
                    if e.kind() == ErrorKind::WouldBlock
                        || e.kind() == ErrorKind::TimedOut
                        || (e.kind() == ErrorKind::Other && e.raw_os_error() == Some(41)) =>
                {
                    sleep().await;
                }
                Err(e) => return Err(e),
            }
        }
    }

    /// Writes all bytes in `buf` to the socket.
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::write`].
    pub async fn write_all(&mut self, mut buf: &[u8]) -> Result<(), std::io::Error> {
        while !buf.is_empty() {
            match self.std_stream.write(buf) {
                Ok(0) => {}
                Ok(num_written) => {
                    buf = &buf[num_written..];
                }
                Err(e)
                    if e.kind() == ErrorKind::WouldBlock
                        || e.kind() == ErrorKind::TimedOut
                        || (e.kind() == ErrorKind::Other && e.raw_os_error() == Some(41)) =>
                {
                    sleep().await;
                }
                Err(e) => return Err(e),
            }
        }
        Ok(())
    }

    /// Writes data from a slice of buffers.
    ///
    /// Takes data from each buffer in order.  May partially read the last buffer read.
    ///
    /// Returns the number of bytes written.
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpStream::write_vectored`].
    pub async fn write_vectored(
        &mut self,
        bufs: &[std::io::IoSlice<'_>],
    ) -> Result<usize, std::io::Error> {
        loop {
            match self.std_stream.write_vectored(bufs) {
                Ok(num) => return Ok(num),
                Err(e)
                    if e.kind() == ErrorKind::WouldBlock
                        || e.kind() == ErrorKind::TimedOut
                        || (e.kind() == ErrorKind::Other && e.raw_os_error() == Some(41)) =>
                {
                    sleep().await;
                }
                Err(e) => return Err(e),
            }
        }
    }
}
