use crate::net::{sleep, TcpStream};
use std::io::ErrorKind;
use std::net::{SocketAddr, ToSocketAddrs};

/// Async wrapper around
/// [`std::net::TcpListener`].
///
/// You can borrow the inner struct with
/// [`inner`](#method.inner) and [`inner_mut`](#method.inner_mut).
#[derive(Debug)]
pub struct TcpListener {
    std_listener: std::net::TcpListener,
}

impl TcpListener {
    /// Wraps an existing listener socket so we can perform async operations on it.
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpListener::set_nonblocking`].
    pub fn new(std_listener: std::net::TcpListener) -> Result<Self, std::io::Error> {
        std_listener.set_nonblocking(true)?;
        crate::timer::start_timer_thread();
        Ok(TcpListener { std_listener })
    }

    /// Returns a TCP listener socket, bound to `addr`, that is ready to accept connections.
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpListener::bind`].
    pub fn bind<A: ToSocketAddrs>(addr: A) -> Result<TcpListener, std::io::Error> {
        // Err(std::io::Error::new(ErrorKind::Other, "err1"));
        TcpListener::new(std::net::TcpListener::bind(addr)?)
    }

    #[must_use]
    pub fn inner(&self) -> &std::net::TcpListener {
        &self.std_listener
    }

    #[must_use]
    pub fn into_inner(self) -> std::net::TcpListener {
        self.std_listener
    }

    /// Makes a new handle to this socket.
    ///
    /// # Errors
    /// Returns any [`std::io::Error`] returned by [`std::net::TcpListener::try_clone`].
    pub fn try_clone(&self) -> Result<TcpListener, std::io::Error> {
        Ok(TcpListener {
            std_listener: self.std_listener.try_clone()?,
        })
    }

    /// Waits for a new connection and then accepts it.
    /// Returns the address of the remote side of the connection
    /// and a stream for reading and writing the connection.
    ///
    /// # Errors
    /// Returns an error if it fails to create a new socket.
    /// This happens when the process runs out of file descriptors.
    pub async fn accept(&self) -> Result<(TcpStream, SocketAddr), std::io::Error> {
        loop {
            match self.std_listener.accept() {
                Ok((std_stream, addr)) => return Ok((TcpStream::new(std_stream)?, addr)),
                Err(e) if e.kind() == ErrorKind::WouldBlock => sleep().await,
                Err(e) => return Err(e),
            }
        }
    }
}
