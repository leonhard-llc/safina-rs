//! A safe Rust library for network communication.
//!
//! # Features
//! - Depends only on `std`
//! - Good test coverage (91%)
//! - Works with any async executor
//!
//! # Limitations
//! - No `AsyncRead` or `AsyncWrite` implementations yet.
//! - Unoptimized.  Uses polling.
//!   This has more latency and CPU load than other async networking libraries.
//! - TCP connect uses a blocking thread.
//!   Concurrent connect operations
//!   are limited by executor's blocking thread pool size.
//!
//! # Examples
//! ```rust
//! # use std::sync::Arc;
//! # use safina::async_test;
//! # use safina::executor::Executor;
//! # #[async_test]
//! # async fn test1() {
//! let bind_addr =
//!     std::net::SocketAddr::from(([127, 0, 0, 1], 0));
//! let mut listener =
//!     safina::net::TcpListener::bind(&bind_addr)
//!     .unwrap();
//! let addr = listener.inner().local_addr().unwrap();
//! println!("{}", &addr);
//!
//! let executor: Arc<Executor> = Arc::default();
//! executor.spawn(async move {
//!     let mut tcp_stream =
//!         safina::net::TcpStream::connect(addr)
//!         .await
//!         .unwrap();
//!     let mut buf = String::new();
//!     tcp_stream.read_to_string(&mut buf).await.unwrap();
//!     println!("read {:?}", buf);
//! });
//!
//! let (mut tcp_stream, _addr)
//!     = listener.accept().await.unwrap();
//! tcp_stream.write_all(b"response").await.unwrap();
//! println!("wrote response");
//! # }
//! ```
//!
//! For complete examples, see the integration tests in
//! [`tests/`](https://gitlab.com/leonhard-llc/safina-rs/-/tree/main/safina/tests).
//!
//! # Alternatives
//! - [async-net](https://crates.io/crates/async-net)
//!   - Dependencies are full of `unsafe`
//! - [async-io](https://crates.io/crates/async-io)
//!   - Full of `unsafe`
//! - [tokio](https://crates.io/crates/tokio)
//!   - Very popular
//!   - Fast
//!   - Internally incredibly complicated
//!   - Full of `unsafe`
//! - [tcp-stream](https://crates.io/crates/tcp-stream)
//!   - Blocks async executor threads
//!   - Contains a little `unsafe` code
//!
//! # TO DO
//! - Add additional features with adapters to popular async io traits.
use core::time::Duration;

mod tcp_stream;
pub use tcp_stream::*;

mod tcp_listener;
pub use tcp_listener::*;

async fn sleep() {
    crate::timer::sleep_for(Duration::from_millis(25)).await;
}
