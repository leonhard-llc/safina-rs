use std::sync::atomic::{AtomicUsize, Ordering};

pub struct AtomicCounter {
    next_value: AtomicUsize,
}
impl Default for AtomicCounter {
    fn default() -> Self {
        Self::new()
    }
}

impl AtomicCounter {
    #[must_use]
    pub fn new() -> Self {
        Self {
            next_value: AtomicUsize::new(0),
        }
    }
    pub fn next(&self) -> usize {
        self.next_value.fetch_add(1, Ordering::AcqRel)
    }
}
