//! Structs for sharing or sending data between async tasks and threads.
//!
//! # Features
//! - [oneshot] and [`sync_channel`] with async and blocking methods
//! - `Mutex` with an async lock method
//! - 100% test coverage
//! - Works with any async executor
//!
//! # Alternatives
//! - [async-lock](https://crates.io/crates/async-lock)
//!   - Contains a little `unsafe` code
//! - [futures-locks](https://crates.io/crates/futures-locks)
//!   - Contains a little `unsafe` code
//! - [futures-util](https://crates.io/crates/futures-util)
//!   - Very popular
//!   - Full of `unsafe`
//! - [tokio-sync](https://crates.io/crates/tokio-sync)
//!   - Very popular
//!   - Fast
//!   - Internally incredibly complicated
//!   - Full of `unsafe`
//!
//! # TO DO
//! - Add an example of a channel between a task and a thread.
//! - Add blocking `Mutex::lock`
//! - Add `Barrier`
//! - Add `RwLock`
//! - Add `WaitableBool`
//! - Add `UnboundedChannel`
//! - Add `WaitableQueue` (multiple receivers)
//! - Add `UnboundedWaitableQueue`
//! - Add `Topic` (copies message to every receiver)
mod mutex;
pub use mutex::*;

mod channel;
pub use channel::*;
