//! Await multiple futures and get the value of the first one that completes.
//!
//! # Features
//! - Good test coverage (96%)
//! - Works with any async executor
//!
//! # Limitations
//! - Can await 2-5 futures.  Nest them if you need more.
//!
//! # Examples
//! ```rust
//! use safina::async_test;
//! use safina::select::{select_ab, OptionAb};
//! # async fn make_new(_: String) -> Result<(), String> { Ok(()) }
//! # async fn get_from_pool(_: String) -> Result<(), String> { Ok(()) }
//! # #[async_test]
//! # async fn test1() -> Result<(), String> {
//! #     let addr = String::new();
//! let conn = match select_ab(make_new(addr.clone()), get_from_pool(addr.clone())).await {
//!     OptionAb::A(result) => result?,
//!     OptionAb::B(result) => result?,
//! };
//! // When both futures return the same type, you can use `take`:
//! let conn = select_ab(make_new(addr.clone()), get_from_pool(addr.clone())).await.take()?;
//! #     Ok(())
//! # }
//! ```
//! ```rust
//! use safina::async_test;
//! use safina::select::{select_ab, OptionAb};
//! # async fn read_data() -> Result<(), String> { Ok(()) }
//! # #[async_test]
//! # async fn test1() -> Result<(), String> {
//! #     let deadline = std::time::Instant::now();
//! safina::timer::start_timer_thread();
//! let data = match select_ab(read_data(), safina::timer::sleep_until(deadline)).await {
//!     OptionAb::A(result) => Ok(result?),
//!     OptionAb::B(()) => Err("timeout"),
//! };
//! #     Ok(())
//! # }
//! ```
//!
//! # Alternatives
//! TODO: Update this section.
//! - [`tokio::select`](https://docs.rs/tokio/latest/tokio/macro.select.html)
//!   - very popular
//!   - Fast
//!   - internally incredibly complicated
//!   - full of `unsafe`
//! - [`futures::select`](https://docs.rs/futures/latest/futures/macro.select.html)
//!   - very popular
//!   - proc macro, very complicated
//!   - contains a little `unsafe` code
#![allow(clippy::many_single_char_names)]
#![allow(clippy::module_name_repetitions)]

mod option_ab;
mod option_abc;
mod option_abcd;
mod option_abcde;
mod select_future;
pub use option_ab::*;
pub use option_abc::*;
pub use option_abcd::*;
pub use option_abcde::*;
pub use select_future::*;

use core::future::Future;
use core::pin::Pin;
use core::task::{Context, Poll};

/// A future that never completes.
pub struct PendingFuture;
impl Unpin for PendingFuture {}
impl Future for PendingFuture {
    type Output = ();
    fn poll(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
        Poll::Pending
    }
}

/// Awaits both futures and returns the value from the one that completes first.
///
/// First moves them to the heap, to make them [Unpin].
/// Use [`SelectFuture::new`] to avoid allocating on the heap.
#[allow(clippy::missing_panics_doc)]
pub async fn select_ab<A, B, FutA, FutB>(a: FutA, b: FutB) -> OptionAb<A, B>
where
    FutA: Future<Output = A> + Send + 'static,
    FutB: Future<Output = B> + Send + 'static,
{
    match SelectFuture::new(
        // TODO: Use Box::into_pin.
        Box::pin(a),
        Box::pin(b),
        PendingFuture {},
        PendingFuture {},
        PendingFuture {},
    )
    .await
    {
        OptionAbcde::A(value) => OptionAb::A(value),
        OptionAbcde::B(value) => OptionAb::B(value),
        _ => unreachable!(),
    }
}

/// Awaits the futures and returns the value from the one that completes first.
///
/// First moves them to the heap, to make them [Unpin].
/// Use [`SelectFuture::new`] to avoid allocating on the heap.
#[allow(clippy::missing_panics_doc)]
pub async fn select_abc<A, B, C, FutA, FutB, FutC>(a: FutA, b: FutB, c: FutC) -> OptionAbc<A, B, C>
where
    FutA: Future<Output = A> + Send + 'static,
    FutB: Future<Output = B> + Send + 'static,
    FutC: Future<Output = C> + Send + 'static,
{
    match SelectFuture::new(
        Box::pin(a),
        Box::pin(b),
        Box::pin(c),
        PendingFuture {},
        PendingFuture {},
    )
    .await
    {
        OptionAbcde::A(value) => OptionAbc::A(value),
        OptionAbcde::B(value) => OptionAbc::B(value),
        OptionAbcde::C(value) => OptionAbc::C(value),
        _ => unreachable!(),
    }
}

/// Awaits the futures and returns the value from the one that completes first.
///
/// First moves them to the heap, to make them [Unpin].
/// Use [`SelectFuture::new`] to avoid allocating on the heap.
#[allow(clippy::missing_panics_doc)]
pub async fn select_abcd<A, B, C, D, FutA, FutB, FutC, FutD>(
    a: FutA,
    b: FutB,
    c: FutC,
    d: FutD,
) -> OptionAbcd<A, B, C, D>
where
    FutA: Future<Output = A> + Send + 'static,
    FutB: Future<Output = B> + Send + 'static,
    FutC: Future<Output = C> + Send + 'static,
    FutD: Future<Output = D> + Send + 'static,
{
    match SelectFuture::new(
        Box::pin(a),
        Box::pin(b),
        Box::pin(c),
        Box::pin(d),
        PendingFuture {},
    )
    .await
    {
        OptionAbcde::A(value) => OptionAbcd::A(value),
        OptionAbcde::B(value) => OptionAbcd::B(value),
        OptionAbcde::C(value) => OptionAbcd::C(value),
        OptionAbcde::D(value) => OptionAbcd::D(value),
        OptionAbcde::E(()) => unreachable!(),
    }
}

/// Awaits the futures and returns the value from the one that completes first.
///
/// First moves them to the heap, to make them [Unpin].
/// Use [`SelectFuture::new`] to avoid allocating on the heap.
pub async fn select_abcde<A, B, C, D, E, FutA, FutB, FutC, FutD, FutE>(
    a: FutA,
    b: FutB,
    c: FutC,
    d: FutD,
    e: FutE,
) -> OptionAbcde<A, B, C, D, E>
where
    FutA: Future<Output = A> + Send + 'static,
    FutB: Future<Output = B> + Send + 'static,
    FutC: Future<Output = C> + Send + 'static,
    FutD: Future<Output = D> + Send + 'static,
    FutE: Future<Output = E> + Send + 'static,
{
    SelectFuture::new(
        Box::pin(a),
        Box::pin(b),
        Box::pin(c),
        Box::pin(d),
        Box::pin(e),
    )
    .await
}
