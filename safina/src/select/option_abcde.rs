use core::fmt::{Debug, Display, Formatter};

pub enum OptionAbcde<A, B, C, D, E> {
    A(A),
    B(B),
    C(C),
    D(D),
    E(E),
}

impl<T> OptionAbcde<T, T, T, T, T> {
    pub fn take(self) -> T {
        match self {
            OptionAbcde::A(value)
            | OptionAbcde::B(value)
            | OptionAbcde::C(value)
            | OptionAbcde::D(value)
            | OptionAbcde::E(value) => value,
        }
    }
}

impl<A, B, C, D, E> OptionAbcde<A, B, C, D, E> {
    pub fn as_ref(&self) -> OptionAbcde<&A, &B, &C, &D, &E> {
        match self {
            OptionAbcde::A(value) => OptionAbcde::A(value),
            OptionAbcde::B(value) => OptionAbcde::B(value),
            OptionAbcde::C(value) => OptionAbcde::C(value),
            OptionAbcde::D(value) => OptionAbcde::D(value),
            OptionAbcde::E(value) => OptionAbcde::E(value),
        }
    }
    pub fn a(&self) -> Option<&A> {
        match self {
            OptionAbcde::A(value) => Some(value),
            _ => None,
        }
    }
    pub fn b(&self) -> Option<&B> {
        match self {
            OptionAbcde::B(value) => Some(value),
            _ => None,
        }
    }
    pub fn c(&self) -> Option<&C> {
        match self {
            OptionAbcde::C(value) => Some(value),
            _ => None,
        }
    }
    pub fn d(&self) -> Option<&D> {
        match self {
            OptionAbcde::D(value) => Some(value),
            _ => None,
        }
    }
    pub fn e(&self) -> Option<&E> {
        match self {
            OptionAbcde::E(value) => Some(value),
            _ => None,
        }
    }
}
impl<A: Debug, B: Debug, C: Debug, D: Debug, E: Debug> OptionAbcde<A, B, C, D, E> {
    /// # Panics
    /// Panics if `self` is not an `OptionABCDE::A`.
    pub fn unwrap_a(self) -> A {
        match self {
            OptionAbcde::A(value) => value,
            _ => panic!("expected OptionABCDE::A(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABCDE::B`.
    pub fn unwrap_b(self) -> B {
        match self {
            OptionAbcde::B(value) => value,
            _ => panic!("expected OptionABCDE::B(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABCDE::C`.
    pub fn unwrap_c(self) -> C {
        match self {
            OptionAbcde::C(value) => value,
            _ => panic!("expected OptionABCDE::C(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABCDE::D`.
    pub fn unwrap_d(self) -> D {
        match self {
            OptionAbcde::D(value) => value,
            _ => panic!("expected OptionABCDE::D(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABCDE::E`.
    pub fn unwrap_e(self) -> E {
        match self {
            OptionAbcde::E(value) => value,
            _ => panic!("expected OptionABCDE::E(_) but found {:?}", self),
        }
    }
}

impl<A: Clone, B: Clone, C: Clone, D: Clone, E: Clone> Clone for OptionAbcde<A, B, C, D, E> {
    fn clone(&self) -> Self {
        match self {
            OptionAbcde::A(value) => OptionAbcde::A(value.clone()),
            OptionAbcde::B(value) => OptionAbcde::B(value.clone()),
            OptionAbcde::C(value) => OptionAbcde::C(value.clone()),
            OptionAbcde::D(value) => OptionAbcde::D(value.clone()),
            OptionAbcde::E(value) => OptionAbcde::E(value.clone()),
        }
    }
}

impl<A: Debug, B: Debug, C: Debug, D: Debug, E: Debug> Debug for OptionAbcde<A, B, C, D, E> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            OptionAbcde::A(value) => write!(f, "OptionABCDE::A({value:?})"),
            OptionAbcde::B(value) => write!(f, "OptionABCDE::B({value:?})"),
            OptionAbcde::C(value) => write!(f, "OptionABCDE::C({value:?})"),
            OptionAbcde::D(value) => write!(f, "OptionABCDE::D({value:?})"),
            OptionAbcde::E(value) => write!(f, "OptionABCDE::E({value:?})"),
        }
    }
}

impl<A: Display, B: Display, C: Display, D: Display, E: Display> Display
    for OptionAbcde<A, B, C, D, E>
{
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            OptionAbcde::A(value) => write!(f, "{value}"),
            OptionAbcde::B(value) => write!(f, "{value}"),
            OptionAbcde::C(value) => write!(f, "{value}"),
            OptionAbcde::D(value) => write!(f, "{value}"),
            OptionAbcde::E(value) => write!(f, "{value}"),
        }
    }
}

impl<A: PartialEq, B: PartialEq, C: PartialEq, D: PartialEq, E: PartialEq> PartialEq
    for OptionAbcde<A, B, C, D, E>
{
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (OptionAbcde::A(value), OptionAbcde::A(other)) if value == other => true,
            (OptionAbcde::B(value), OptionAbcde::B(other)) if value == other => true,
            (OptionAbcde::C(value), OptionAbcde::C(other)) if value == other => true,
            (OptionAbcde::D(value), OptionAbcde::D(other)) if value == other => true,
            (OptionAbcde::E(value), OptionAbcde::E(other)) if value == other => true,
            _ => false,
        }
    }
}
impl<A: PartialEq, B: PartialEq, C: PartialEq, D: PartialEq, E: PartialEq> Eq
    for OptionAbcde<A, B, C, D, E>
{
}
