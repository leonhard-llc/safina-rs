use core::fmt::{Debug, Display, Formatter};

pub enum OptionAbcd<A, B, C, D> {
    A(A),
    B(B),
    C(C),
    D(D),
}

impl<T> OptionAbcd<T, T, T, T> {
    pub fn take(self) -> T {
        match self {
            OptionAbcd::A(value)
            | OptionAbcd::B(value)
            | OptionAbcd::C(value)
            | OptionAbcd::D(value) => value,
        }
    }
}

impl<A, B, C, D> OptionAbcd<A, B, C, D> {
    pub fn as_ref(&self) -> OptionAbcd<&A, &B, &C, &D> {
        match self {
            OptionAbcd::A(value) => OptionAbcd::A(value),
            OptionAbcd::B(value) => OptionAbcd::B(value),
            OptionAbcd::C(value) => OptionAbcd::C(value),
            OptionAbcd::D(value) => OptionAbcd::D(value),
        }
    }
    pub fn a(&self) -> Option<&A> {
        match self {
            OptionAbcd::A(value) => Some(value),
            _ => None,
        }
    }
    pub fn b(&self) -> Option<&B> {
        match self {
            OptionAbcd::B(value) => Some(value),
            _ => None,
        }
    }
    pub fn c(&self) -> Option<&C> {
        match self {
            OptionAbcd::C(value) => Some(value),
            _ => None,
        }
    }
    pub fn d(&self) -> Option<&D> {
        match self {
            OptionAbcd::D(value) => Some(value),
            _ => None,
        }
    }
}
impl<A: Debug, B: Debug, C: Debug, D: Debug> OptionAbcd<A, B, C, D> {
    /// # Panics
    /// Panics if `self` is not an `OptionABCD::A`.
    pub fn unwrap_a(self) -> A {
        match self {
            OptionAbcd::A(value) => value,
            _ => panic!("expected OptionABCD::A(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABCD::B`.
    pub fn unwrap_b(self) -> B {
        match self {
            OptionAbcd::B(value) => value,
            _ => panic!("expected OptionABCD::B(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABCD::C`.
    pub fn unwrap_c(self) -> C {
        match self {
            OptionAbcd::C(value) => value,
            _ => panic!("expected OptionABCD::C(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABCD::D`.
    pub fn unwrap_d(self) -> D {
        match self {
            OptionAbcd::D(value) => value,
            _ => panic!("expected OptionABCD::D(_) but found {:?}", self),
        }
    }
}

impl<A: Clone, B: Clone, C: Clone, D: Clone> Clone for OptionAbcd<A, B, C, D> {
    fn clone(&self) -> Self {
        match self {
            OptionAbcd::A(value) => OptionAbcd::A(value.clone()),
            OptionAbcd::B(value) => OptionAbcd::B(value.clone()),
            OptionAbcd::C(value) => OptionAbcd::C(value.clone()),
            OptionAbcd::D(value) => OptionAbcd::D(value.clone()),
        }
    }
}

impl<A: Debug, B: Debug, C: Debug, D: Debug> Debug for OptionAbcd<A, B, C, D> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            OptionAbcd::A(value) => write!(f, "OptionABCD::A({value:?})"),
            OptionAbcd::B(value) => write!(f, "OptionABCD::B({value:?})"),
            OptionAbcd::C(value) => write!(f, "OptionABCD::C({value:?})"),
            OptionAbcd::D(value) => write!(f, "OptionABCD::D({value:?})"),
        }
    }
}

impl<A: Display, B: Display, C: Display, D: Display> Display for OptionAbcd<A, B, C, D> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            OptionAbcd::A(value) => write!(f, "{value}"),
            OptionAbcd::B(value) => write!(f, "{value}"),
            OptionAbcd::C(value) => write!(f, "{value}"),
            OptionAbcd::D(value) => write!(f, "{value}"),
        }
    }
}

impl<A: PartialEq, B: PartialEq, C: PartialEq, D: PartialEq> PartialEq for OptionAbcd<A, B, C, D> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (OptionAbcd::A(value), OptionAbcd::A(other)) if value == other => true,
            (OptionAbcd::B(value), OptionAbcd::B(other)) if value == other => true,
            (OptionAbcd::C(value), OptionAbcd::C(other)) if value == other => true,
            (OptionAbcd::D(value), OptionAbcd::D(other)) if value == other => true,
            _ => false,
        }
    }
}
impl<A: PartialEq, B: PartialEq, C: PartialEq, D: PartialEq> Eq for OptionAbcd<A, B, C, D> {}
