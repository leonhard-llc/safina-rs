use crate::select::OptionAbcde;
use core::future::Future;
use core::pin::Pin;
use core::task::{Context, Poll};

/// A future that polls multiple futures
/// and returns the value from the one that completes first.
///
/// If you want to select fewer futures, pass [`crate::select::PendingFuture`] for any unused future slots.
#[must_use = "futures stay idle unless you await them"]
pub struct SelectFuture<A, B, C, D, E, FutA, FutB, FutC, FutD, FutE>
where
    FutA: Future<Output = A> + Send + Unpin + 'static,
    FutB: Future<Output = B> + Send + Unpin + 'static,
    FutC: Future<Output = C> + Send + Unpin + 'static,
    FutD: Future<Output = D> + Send + Unpin + 'static,
    FutE: Future<Output = E> + Send + Unpin + 'static,
{
    a: FutA,
    b: FutB,
    c: FutC,
    d: FutD,
    e: FutE,
}

impl<A, B, C, D, E, FutA, FutB, FutC, FutD, FutE>
    SelectFuture<A, B, C, D, E, FutA, FutB, FutC, FutD, FutE>
where
    FutA: Future<Output = A> + Send + Unpin + 'static,
    FutB: Future<Output = B> + Send + Unpin + 'static,
    FutC: Future<Output = C> + Send + Unpin + 'static,
    FutD: Future<Output = D> + Send + Unpin + 'static,
    FutE: Future<Output = E> + Send + Unpin + 'static,
{
    /// Makes a future that awaits all the provided futures,
    /// and returns the value from the one that completes first.
    ///
    /// Note that they must be [Unpin].
    /// Use [`Box::pin`] to make them Unpin.
    /// Or use [`pin_utils::pin_mut`](https://docs.rs/pin-utils/latest/pin_utils/macro.pin_mut.html)
    /// to do it with unsafe code that does not allocate memory.
    pub fn new(
        a: FutA,
        b: FutB,
        c: FutC,
        d: FutD,
        e: FutE,
    ) -> SelectFuture<A, B, C, D, E, FutA, FutB, FutC, FutD, FutE> {
        SelectFuture { a, b, c, d, e }
    }
}

impl<A, B, C, D, E, FutA, FutB, FutC, FutD, FutE> Future
    for SelectFuture<A, B, C, D, E, FutA, FutB, FutC, FutD, FutE>
where
    FutA: Future<Output = A> + Send + Unpin + 'static,
    FutB: Future<Output = B> + Send + Unpin + 'static,
    FutC: Future<Output = C> + Send + Unpin + 'static,
    FutD: Future<Output = D> + Send + Unpin + 'static,
    FutE: Future<Output = E> + Send + Unpin + 'static,
{
    type Output = OptionAbcde<A, B, C, D, E>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut_self = self.get_mut();
        // "Note that on multiple calls to poll, only the Waker from the Context
        // passed to the most recent call should be scheduled to receive a wakeup."
        // https://doc.rust-lang.org/stable/std/future/trait.Future.html#tymethod.poll
        // There is a race condition between a worker thread calling a waker and
        // the poll function saving a new waker.
        //
        // With SelectFuture, we can potentially have two worker threads calling the
        // same waker.  The waker could get called multiple times, even after
        // the future has completed.  The docs don't say whether this is allowed
        // or not.  If this becomes a problem, we can add code to prevent it.
        match Pin::new(&mut mut_self.a).poll(cx) {
            Poll::Ready(value) => return Poll::Ready(OptionAbcde::A(value)),
            Poll::Pending => {}
        }
        match Pin::new(&mut mut_self.b).poll(cx) {
            Poll::Ready(value) => return Poll::Ready(OptionAbcde::B(value)),
            Poll::Pending => {}
        }
        match Pin::new(&mut mut_self.c).poll(cx) {
            Poll::Ready(value) => return Poll::Ready(OptionAbcde::C(value)),
            Poll::Pending => {}
        }
        match Pin::new(&mut mut_self.d).poll(cx) {
            Poll::Ready(value) => return Poll::Ready(OptionAbcde::D(value)),
            Poll::Pending => {}
        }
        match Pin::new(&mut mut_self.e).poll(cx) {
            Poll::Ready(value) => return Poll::Ready(OptionAbcde::E(value)),
            Poll::Pending => {}
        }
        Poll::Pending
    }
}
