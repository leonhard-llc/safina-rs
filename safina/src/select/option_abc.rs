use core::fmt::{Debug, Display, Formatter};

pub enum OptionAbc<A, B, C> {
    A(A),
    B(B),
    C(C),
}

impl<T> OptionAbc<T, T, T> {
    pub fn take(self) -> T {
        match self {
            OptionAbc::A(value) | OptionAbc::B(value) | OptionAbc::C(value) => value,
        }
    }
}

impl<A, B, C> OptionAbc<A, B, C> {
    pub fn as_ref(&self) -> OptionAbc<&A, &B, &C> {
        match self {
            OptionAbc::A(value) => OptionAbc::A(value),
            OptionAbc::B(value) => OptionAbc::B(value),
            OptionAbc::C(value) => OptionAbc::C(value),
        }
    }
    pub fn a(&self) -> Option<&A> {
        match self {
            OptionAbc::A(value) => Some(value),
            _ => None,
        }
    }
    pub fn b(&self) -> Option<&B> {
        match self {
            OptionAbc::B(value) => Some(value),
            _ => None,
        }
    }
    pub fn c(&self) -> Option<&C> {
        match self {
            OptionAbc::C(value) => Some(value),
            _ => None,
        }
    }
}
impl<A: Debug, B: Debug, C: Debug> OptionAbc<A, B, C> {
    /// # Panics
    /// Panics if `self` is not an `OptionABC::A`.
    pub fn unwrap_a(self) -> A {
        match self {
            OptionAbc::A(value) => value,
            _ => panic!("expected OptionABC::A(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABC::B`.
    pub fn unwrap_b(self) -> B {
        match self {
            OptionAbc::B(value) => value,
            _ => panic!("expected OptionABC::B(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionABC::C`.
    pub fn unwrap_c(self) -> C {
        match self {
            OptionAbc::C(value) => value,
            _ => panic!("expected OptionABC::C(_) but found {:?}", self),
        }
    }
}

impl<A: Clone, B: Clone, C: Clone> Clone for OptionAbc<A, B, C> {
    fn clone(&self) -> Self {
        match self {
            OptionAbc::A(value) => OptionAbc::A(value.clone()),
            OptionAbc::B(value) => OptionAbc::B(value.clone()),
            OptionAbc::C(value) => OptionAbc::C(value.clone()),
        }
    }
}

impl<A: Debug, B: Debug, C: Debug> Debug for OptionAbc<A, B, C> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            OptionAbc::A(value) => write!(f, "OptionABC::A({value:?})"),
            OptionAbc::B(value) => write!(f, "OptionABC::B({value:?})"),
            OptionAbc::C(value) => write!(f, "OptionABC::C({value:?})"),
        }
    }
}

impl<A: Display, B: Display, C: Display> Display for OptionAbc<A, B, C> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            OptionAbc::A(value) => write!(f, "{value}"),
            OptionAbc::B(value) => write!(f, "{value}"),
            OptionAbc::C(value) => write!(f, "{value}"),
        }
    }
}

impl<A: PartialEq, B: PartialEq, C: PartialEq> PartialEq for OptionAbc<A, B, C> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (OptionAbc::A(value), OptionAbc::A(other)) if value == other => true,
            (OptionAbc::B(value), OptionAbc::B(other)) if value == other => true,
            (OptionAbc::C(value), OptionAbc::C(other)) if value == other => true,
            _ => false,
        }
    }
}
impl<A: PartialEq, B: PartialEq, C: PartialEq> Eq for OptionAbc<A, B, C> {}
