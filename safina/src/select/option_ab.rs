use core::fmt::{Debug, Display, Formatter};

pub enum OptionAb<A, B> {
    A(A),
    B(B),
}

impl<T> OptionAb<T, T> {
    pub fn take(self) -> T {
        match self {
            OptionAb::A(value) | OptionAb::B(value) => value,
        }
    }
}

impl<A, B> OptionAb<A, B> {
    pub fn as_ref(&self) -> OptionAb<&A, &B> {
        match self {
            OptionAb::A(value) => OptionAb::A(value),
            OptionAb::B(value) => OptionAb::B(value),
        }
    }
    pub fn a(&self) -> Option<&A> {
        match self {
            OptionAb::A(value) => Some(value),
            OptionAb::B(_) => None,
        }
    }
    pub fn b(&self) -> Option<&B> {
        match self {
            OptionAb::A(_) => None,
            OptionAb::B(value) => Some(value),
        }
    }
}

impl<A: Debug, B: Debug> OptionAb<A, B> {
    /// # Panics
    /// Panics if `self` is not an `OptionAB::A`.
    pub fn unwrap_a(self) -> A {
        match self {
            OptionAb::A(value) => value,
            OptionAb::B(_) => panic!("expected OptionAB::A(_) but found {:?}", self),
        }
    }
    /// # Panics
    /// Panics if `self` is not an `OptionAB::B`.
    pub fn unwrap_b(self) -> B {
        match self {
            OptionAb::A(_) => panic!("expected OptionAB::B(_) but found {:?}", self),
            OptionAb::B(value) => value,
        }
    }
}

impl<A: Clone, B: Clone> Clone for OptionAb<A, B> {
    fn clone(&self) -> Self {
        match self {
            OptionAb::A(value) => OptionAb::A(value.clone()),
            OptionAb::B(value) => OptionAb::B(value.clone()),
        }
    }
}

impl<A: Debug, B: Debug> Debug for OptionAb<A, B> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            OptionAb::A(value) => write!(f, "OptionAB::A({value:?})"),
            OptionAb::B(value) => write!(f, "OptionAB::B({value:?})"),
        }
    }
}

impl<A: Display, B: Display> Display for OptionAb<A, B> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            OptionAb::A(value) => write!(f, "{value}"),
            OptionAb::B(value) => write!(f, "{value}"),
        }
    }
}

impl<A: PartialEq, B: PartialEq> PartialEq for OptionAb<A, B> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (OptionAb::A(value), OptionAb::A(other)) if value == other => true,
            (OptionAb::B(value), OptionAb::B(other)) if value == other => true,
            _ => false,
        }
    }
}
impl<A: PartialEq, B: PartialEq> Eq for OptionAb<A, B> {}
