pub use crate::sync::Receiver;
pub use crate::threadpool::NewThreadPoolError;
use crate::threadpool::ThreadPool;
use core::cell::Cell;
use core::future::Future;
use core::pin::Pin;
use core::task::Poll;
use std::sync::mpsc::SyncSender;
use std::sync::{Arc, Mutex, Weak};

thread_local! {
    static EXECUTOR: Cell<Weak<Executor>> = const { Cell::new(Weak::new()) };
}

/// Gets the [Executor] from thread-local storage.
///
/// This is a low-level function.
/// You probably want to call [spawn] and other functions which use this function internally.
#[allow(clippy::module_name_repetitions)]
#[must_use]
pub fn get_thread_executor() -> Option<Arc<Executor>> {
    EXECUTOR.with(|cell| {
        let weak = cell.take();
        let result = weak.upgrade();
        cell.set(weak);
        result
    })
}

/// Guard returned by [`set_thread_executor`].
///
/// On drop, it removes the thread-local reference to the executor.
pub struct ThreadExecutorGuard;
impl Drop for ThreadExecutorGuard {
    fn drop(&mut self) {
        EXECUTOR.with(Cell::take);
    }
}

/// Sets `executor` as the [Executor] for the current thread,
/// saving it to thread-local storage.
///
/// Calls to [spawn] and other crate-level
/// functions will use this executor.
///
/// Returns a guard struct.
/// When the guard drops, it removes `executor` from thread-local storage.
///
/// This is a low-level function.  You probably don't need to use this.
#[allow(clippy::module_name_repetitions)]
#[must_use]
pub fn set_thread_executor(executor: Weak<Executor>) -> ThreadExecutorGuard {
    EXECUTOR.set(executor);
    ThreadExecutorGuard {}
}

/// Executor constructor parameters.
#[allow(clippy::module_name_repetitions)]
pub struct ExecutorBuilder {
    pub thread_name_prefix: String,
    pub num_async_threads: usize,
    pub num_blocking_threads: usize,
}
impl Default for ExecutorBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl ExecutorBuilder {
    /// Creates a builder with default settings:
    /// - 4 async threads, named `"async0"` through `"async3"`
    /// - 4 blocking threads, named `"blocking0"` through `"blocking3"`
    ///
    /// These are equivalent:
    /// - `let executor = Executor::builder().build().unwrap()`
    /// - `let executor: Arc<Executor> = Arc::default()`
    #[must_use]
    pub fn new() -> Self {
        Self {
            thread_name_prefix: String::new(),
            num_async_threads: 4,
            num_blocking_threads: 4,
        }
    }

    /// Sets the number of threads that will execute async tasks.
    /// `n` must be greater than zero.
    #[must_use]
    pub fn with_async_threads(mut self, n: usize) -> Self {
        self.num_async_threads = n;
        self
    }

    /// Sets the number of threads that will execute blocking jobs.
    /// `n` must be greater than zero.
    #[must_use]
    pub fn with_blocking_threads(mut self, n: usize) -> Self {
        self.num_blocking_threads = n;
        self
    }

    /// Sets the prefix of executor thread names.
    /// When `name_prefix` is `"t_"` then the threads use names
    /// `"t_async0"`, `"t_async1"`, `"t_blocking0"`, `"t_io"`, and so on.
    #[must_use]
    pub fn with_thread_name_prefix(mut self, name_prefix: impl Into<String>) -> Self {
        self.thread_name_prefix = name_prefix.into();
        self
    }

    /// Creates the executor and starts the threads.
    ///
    /// # Errors
    /// Returns an error when a parameter is invalid or it fails to start threads.
    pub fn build(self) -> Result<Arc<Executor>, NewThreadPoolError> {
        Executor::build(&self)
    }
}

/// A collection of threads for executing async tasks and blocking jobs.
///
/// The methods of this struct are defined on `&Arc<Executor>`.
///
/// # Features
/// - Spawn async tasks to execute on a threadpool
/// - Separate threadpool for running blocking closures or `FnOnce`
/// - Supports multiple executors
/// - Good test coverage (100%)
///
/// # Limitations
/// - Not optimized
///
/// # Examples
/// ```rust
/// use std::sync::Arc;
/// use safina::executor::Executor;
///
/// let executor: Arc<Executor> = Arc::default();
/// let (sender, receiver) = std::sync::mpsc::channel();
/// executor.spawn(async move {
///     sender.send(()).unwrap();
/// });
/// receiver.recv().unwrap();
/// ```
///
/// ```rust
/// # async fn prepare_request() -> Result<(), std::io::Error> { Ok(()) }
/// # async fn execute_request() -> Result<(), std::io::Error> { Ok(()) }
/// # fn f() -> Result<(), std::io::Error> {
/// let result = safina::executor::block_on(async {
///     prepare_request().await?;
///     execute_request().await
/// })?;
/// # Ok(())
/// # }
/// # f().unwrap()
/// ```
///
/// ```rust
/// # fn read_file1() -> Result<(), std::io::Error> { Ok(()) }
/// # fn read_file2() -> Result<(), std::io::Error> { Ok(()) }
/// # async fn f() -> Result<(), std::io::Error> {
/// let result = safina::executor::schedule_blocking(|| {
///     read_file1()?;
///     read_file2()
/// }).async_recv().await.unwrap()?;
/// # Ok(())
/// # }
/// # let executor = safina::executor::Executor::new(1,1).unwrap();
/// # executor.block_on(f()).unwrap();
/// ```
///
/// # Alternatives
/// - [async-executor](https://crates.io/crates/async-executor)
///   - Popular
///   - Dependencies have some `unsafe` code
/// - [futures-executor](https://crates.io/crates/futures-executor)
///   - Very popular
///   - Full of `unsafe`
/// - [tokio-executor](https://crates.io/crates/tokio-executor)
///   - Very popular
///   - Fast
///   - Internally complicated
///   - Full of `unsafe`
/// - [executors](https://crates.io/crates/executors)
///   - Dependencies have lots of `unsafe` code
/// - [bastion-executor](https://crates.io/crates/bastion-executor)
///   - Lots of `unsafe` code
/// - [`rayon_core`](https://crates.io/crates/rayon-core)
///   - Generous amounts of `unsafe` code
/// - [pollster](https://crates.io/crates/pollster)
///   - Minimal
///   - Has a little `unsafe` code
/// - [lelet](https://crates.io/crates/lelet)
///   - Automatically scales worker thread pool
///   - Lots of `unsafe` code
/// - [fibers](https://crates.io/crates/fibers)
///   - Dependencies are full of unsafe
/// - [`nostd_async`](https://crates.io/crates/nostd_async)
///   - Has some `unsafe` code
/// - [embedded-executor](https://crates.io/crates/embedded-executor)
///   - Generous amounts of `unsafe` code
/// - [`spin_on`](https://crates.io/crates/spin_on)
///   - Minimal
/// - [pasts](https://crates.io/crates/pasts)
/// - [switchyard](https://crates.io/crates/switchyard)
///   - Lots of `unsafe` code
/// - [sealrs](https://crates.io/crates/sealrs)
/// - [`rusty_pool`](https://crates.io/crates/rusty_pool)
///   - Automatically adjusts thread pool
///   - Dependencies are full of unsafe
///
/// # TO DO
/// - Add a stress test
/// - Add a benchmark.  See benchmarks in <https://crates.io/crates/executors>
/// - Add a `#[safina_main]` macro
/// - Look into using [flume](https://crates.io/crates/flume)
///   to eliminate the receiver mutex and reduce contention.
pub struct Executor {
    async_pool: ThreadPool,
    blocking_pool: ThreadPool,
}

impl Executor {
    /// Creates a new builder with default settings.  See [`ExecutorBuilder::new`].
    #[must_use]
    pub fn builder() -> ExecutorBuilder {
        ExecutorBuilder::new()
    }

    pub(crate) fn build(builder: &ExecutorBuilder) -> Result<Arc<Self>, NewThreadPoolError> {
        Ok(Arc::new(Self {
            async_pool: ThreadPool::new(
                format!("{}async", builder.thread_name_prefix),
                builder.num_async_threads,
            )?,
            blocking_pool: ThreadPool::new(
                format!("{}blocking", builder.thread_name_prefix),
                builder.num_blocking_threads,
            )?,
        }))
    }

    /// Creates a new executor.
    ///
    /// `async_threads` is the number of threads to use for executing async
    /// tasks.
    ///
    /// `blocking_threads` is the number of threads to use for executing
    /// blocking jobs like connecting TCP sockets and reading files.
    ///
    /// Instead of calling this function, you probably want to do:
    /// ```
    /// # use std::sync::Arc;
    /// # use safina::executor::Executor;
    /// let executor: Arc<Executor> = Arc::default();
    /// ```
    ///
    /// # Errors
    /// Returns an error when a parameter is invalid or it fails to start threads.
    pub fn new(
        async_threads: usize,
        blocking_threads: usize,
    ) -> Result<Arc<Self>, NewThreadPoolError> {
        Self::builder()
            .with_async_threads(async_threads)
            .with_blocking_threads(blocking_threads)
            .build()
    }

    /// Schedules `func` to run on any available thread in the blocking thread pool.
    ///
    /// Returns immediately.
    ///
    /// Use the returned receiver to get the result of `func`.
    /// If `func` panics, the receiver returns [`std::sync::mpsc::RecvError`].
    ///
    /// Puts `func` in a [Box] before adding it to the thread pool queue.
    pub fn schedule_blocking<T, F>(self: &Arc<Self>, func: F) -> Receiver<T>
    where
        T: Send + 'static,
        F: (FnOnce() -> T) + Send + 'static,
    {
        let (sender, receiver) = crate::sync::oneshot();
        let weak_self = Arc::downgrade(self);
        self.blocking_pool.schedule(move || {
            let _guard = set_thread_executor(weak_self);
            let _result = sender.send(func());
        });
        receiver
    }

    /// Adds a task that will execute `fut`.
    ///
    /// The task runs on any available worker thread.
    /// The task runs until `fut` completes or the Executor is dropped.
    ///
    /// Returns immediately.
    ///
    /// Uses [`Box::pin`] to make the future [Unpin].
    /// You can use [`spawn_unpin`] to avoid this allocation.
    ///
    /// Example:
    /// ```rust
    /// use std::sync::Arc;
    /// use safina::executor::Executor;
    ///
    /// # async fn an_async_fn() -> Result<(), std::io::Error> { Ok(()) }
    /// # fn f() {
    /// let executor: Arc<Executor> = Arc::default();
    /// executor.spawn(async move {
    ///     an_async_fn().await.unwrap();
    /// });
    /// # }
    /// ```
    pub fn spawn(self: &Arc<Self>, fut: impl (Future<Output = ()>) + Send + 'static) {
        self.spawn_unpin(Box::pin(fut));
    }

    /// Adds a task that will execute `fut`.
    ///
    /// The task runs on any available worker thread.
    /// The task runs until `fut` completes or the Executor is dropped.
    ///
    /// Returns immediately.
    ///
    /// Note that `fut` must be [Unpin].  You can use [`Box::pin`] to make it Unpin.
    /// The [spawn] function does this for you.
    /// Or use [`pin_utils::pin_mut`](https://docs.rs/pin-utils/latest/pin_utils/macro.pin_mut.html)
    /// to do it with unsafe code that does not allocate memory.
    pub fn spawn_unpin(self: &Arc<Self>, fut: impl (Future<Output = ()>) + Send + Unpin + 'static) {
        let task: Arc<Mutex<Option<Box<dyn Future<Output = ()> + Send + Unpin>>>> =
            Arc::new(Mutex::new(Some(Box::new(fut))));
        let weak_self = Arc::downgrade(self);
        self.async_pool.schedule(move || poll_task(task, weak_self));
    }

    /// Executes the future on the current thread and returns its result.
    ///
    /// `fut` can call [spawn] to create tasks.
    /// Those tasks run on the executor and will continue even after
    /// `fut` completes and this call returns.
    ///
    /// Uses [`Box::pin`] to make the future [Unpin].
    /// You can use [`block_on_unpin`] to avoid this allocation.
    ///
    /// # Panics
    /// Panics if the future panics.
    ///
    /// # Example
    /// ```rust
    /// # async fn prepare_request() -> Result<(), std::io::Error> { Ok(()) }
    /// # async fn execute_request() -> Result<(), std::io::Error> { Ok(()) }
    /// # fn f() -> Result<(), std::io::Error> {
    /// use std::sync::Arc;
    /// use safina::executor::Executor;
    ///
    /// let executor: Arc<Executor> = Arc::default();
    /// let result = executor.block_on(async {
    ///     prepare_request().await?;
    ///     execute_request().await
    /// })?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn block_on<R>(self: &Arc<Self>, fut: impl (Future<Output = R>) + 'static) -> R {
        self.block_on_unpin(Box::pin(fut))
    }

    /// Executes the future on the current thread and returns its result.
    ///
    /// `fut` can call [spawn] to create tasks.
    /// Those tasks run on the executor and will continue even after
    /// `fut` completes and this call returns.
    ///
    /// Note that `fut` must be [Unpin].  You can use [`Box::pin`] to make it Unpin.
    /// The [`block_on`] function does this for you.
    /// Or use [`pin_utils::pin_mut`](https://docs.rs/pin-utils/latest/pin_utils/macro.pin_mut.html)
    /// to do it with unsafe code that does not allocate memory.
    ///
    /// # Panics
    /// Panics if the future panics.
    pub fn block_on_unpin<R>(
        self: &Arc<Self>,
        fut: impl (Future<Output = R>) + Unpin + 'static,
    ) -> R {
        let _guard = set_thread_executor(Arc::downgrade(self));
        block_on_unpin(fut)
    }
}

impl Default for Executor {
    fn default() -> Self {
        let arc_executor = Executor::builder().build().unwrap();
        Arc::into_inner(arc_executor).unwrap_or_else(|| unreachable!())
    }
}

/// Schedules `func` to run on any available thread in the blocking thread pool.
///
/// Returns immediately.
///
/// Use the returned receiver to get the result of `func`.
/// If `func` panics, the receiver returns [`std::sync::mpsc::RecvError`].
///
/// Puts `func` in a [Box] before adding it to the thread pool queue.
///
/// # Panics
/// Panics if the caller is not running on an [Executor].
pub fn schedule_blocking<T, F>(func: F) -> Receiver<T>
where
    T: Send + 'static,
    F: (FnOnce() -> T) + Send + 'static,
{
    if let Some(executor) = get_thread_executor() {
        executor.schedule_blocking(func)
    } else {
        // See explanation in `spawn_unpin`.
        panic!(
            "called from outside a task; check for duplicate safina-executor crate: cargo tree -d"
        );
    }
}

#[allow(clippy::needless_pass_by_value)]
fn poll_task(
    task: Arc<Mutex<Option<Box<dyn Future<Output = ()> + Send + Unpin>>>>,
    executor: Weak<Executor>,
) {
    if executor.strong_count() > 0 {
        let waker =
            std::task::Waker::from(Arc::new(TaskWaker::new(task.clone(), executor.clone())));
        let mut cx = std::task::Context::from_waker(&waker);
        let mut opt_fut_guard = task.lock().unwrap();
        if let Some(fut) = opt_fut_guard.as_mut() {
            let _guard = set_thread_executor(executor);
            match Pin::new(&mut *fut).poll(&mut cx) {
                Poll::Ready(()) => {
                    opt_fut_guard.take();
                }
                Poll::Pending => {}
            }
        }
    }
}

// See discussion at https://github.com/rust-lang/rust/issues/42877 .
struct TaskWaker {
    task: Arc<Mutex<Option<Box<dyn Future<Output = ()> + Send + Unpin>>>>,
    executor: Weak<Executor>,
}
impl TaskWaker {
    pub fn new(
        task: Arc<Mutex<Option<Box<dyn Future<Output = ()> + Send + Unpin>>>>,
        executor: Weak<Executor>,
    ) -> Self {
        Self { task, executor }
    }
}
impl std::task::Wake for TaskWaker {
    fn wake(self: Arc<Self>) {
        if let Some(ref executor) = self.executor.upgrade() {
            let task_clone = self.task.clone();
            let executor_weak = Arc::downgrade(executor);
            executor
                .async_pool
                .schedule(move || poll_task(task_clone, executor_weak));
        }
    }
}

/// Creates a new task to execute `fut` and schedules it for immediate execution.
///
/// Returns immediately.
///
/// Uses [`Box::pin`] to make the future [Unpin].
/// You can use [`spawn_unpin`] to avoid this allocation.
///
/// # Panics
/// Panics if the caller is not running on an [Executor].
///
/// # Example
/// ```rust
/// # async fn an_async_fn() -> Result<(), std::io::Error> { Ok(()) }
/// # fn f() {
/// use std::sync::Arc;
/// use safina::executor::Executor;
///
/// let executor: Arc<Executor> = Arc::default();
/// executor.spawn(async move {
///     safina::executor::spawn(async move {
///         an_async_fn().await.unwrap();
///     });
/// });
/// # }
/// ```
pub fn spawn(fut: impl (Future<Output = ()>) + Send + 'static) {
    spawn_unpin(Box::pin(fut));
}

/// Creates a new task to execute `fut` and schedules it for immediate execution.
///
/// Returns immediately.
///
/// Note that `fut` must be [Unpin].  You can use [`Box::pin`] to make it Unpin.
/// The [spawn] function does this for you.
/// Or use [`pin_utils::pin_mut`](https://docs.rs/pin-utils/latest/pin_utils/macro.pin_mut.html)
/// to do it with unsafe code that does not allocate memory.
///
/// # Panics
/// Panics if the caller is not running on an [Executor].
pub fn spawn_unpin(fut: impl (Future<Output = ()>) + Send + Unpin + 'static) {
    if let Some(executor) = get_thread_executor() {
        let task: Arc<Mutex<Option<Box<dyn Future<Output = ()> + Send + Unpin>>>> =
            Arc::new(Mutex::new(Some(Box::new(fut))));
        let executor_weak = Arc::downgrade(&executor);
        executor
            .async_pool
            .schedule(move || poll_task(task, executor_weak));
    } else {
        // Note: This panic can happen when there are two versions of the safina-executor crate.
        // Example:  You use the `async_test` macro.  It creates an executor with the first
        // version of the crate.  The executor is stored in the first crate's thread-local storage.
        // The second crate's thread-local storage is empty.  If the test code calls the second
        // crate's spawn or schedule functions, it hits this error.
        panic!(
            "called from outside a task; check for duplicate safina-executor crate: cargo tree -d"
        );
    }
}

/// Executes the future on the current thread and returns its result.
///
/// Does not create an executor.
/// Use [`Executor::block_on`] if you need `fut` to spawn new tasks.
///
/// Uses [`Box::pin`] to make the future [Unpin].
/// You can use [`block_on_unpin`] to avoid this allocation.
///
/// # Panics
/// Panics if the future panics.
///
/// Panics if `fut` calls [spawn] to create a new task.
///
/// # Example
/// ```rust
/// # async fn prepare_request() -> Result<(), std::io::Error> { Ok(()) }
/// # async fn execute_request() -> Result<(), std::io::Error> { Ok(()) }
/// # fn f() -> Result<(), std::io::Error> {
/// let result = safina::executor::block_on(async {
///     prepare_request().await?;
///     execute_request().await
/// })?;
/// # Ok(())
/// # }
/// ```
pub fn block_on<R>(fut: impl (Future<Output = R>) + 'static) -> R {
    block_on_unpin(Box::pin(fut))
}

/// Executes the future on the current thread and returns its result.
///
/// Does not create an executor.
/// Use [`Executor::block_on`] if you need `fut` to spawn new tasks.
///
/// Note that `fut` must be [Unpin].  You can use [`Box::pin`] to make it Unpin.
/// The [`block_on`] function does this for you.
/// Or use [`pin_utils::pin_mut`](https://docs.rs/pin-utils/latest/pin_utils/macro.pin_mut.html)
/// to do it with unsafe code that does not allocate memory.
///
/// # Panics
/// Panics if the future panics.
///
/// Panics if `fut` calls [spawn] to create a new task.
pub fn block_on_unpin<R>(mut fut: impl (Future<Output = R>) + Unpin + 'static) -> R {
    struct BlockOnTaskWaker(Mutex<Option<SyncSender<()>>>);
    impl std::task::Wake for BlockOnTaskWaker {
        fn wake(self: Arc<Self>) {
            if let Some(sender) = self.0.lock().unwrap().take() {
                let _ = sender.send(());
            }
        }
    }
    loop {
        let (sender, receiver) = std::sync::mpsc::sync_channel(1);
        let waker = std::task::Waker::from(Arc::new(BlockOnTaskWaker(Mutex::new(Some(sender)))));
        let mut cx = std::task::Context::from_waker(&waker);
        if let Poll::Ready(result) = Pin::new(&mut fut).poll(&mut cx) {
            return result;
        }
        receiver.recv().unwrap();
    }
}
