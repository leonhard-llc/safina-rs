//! Provides async [`sleep_for`] and [`sleep_until`] functions.
//!
//! # Features
//! - Depends only on `std`
//! - 100% test coverage
//! - Source of time is [`std::thread::park_timeout`] via [`std::sync::mpsc::Receiver::recv_timeout`].
//! - Works any async executor
//!
//! # Limitations
//! - Timers complete 1-2ms late, but never early
//!
//! # Examples
//! ```rust
//! # use core::time::Duration;
//! # async fn f() {
//! safina::timer::start_timer_thread();
//! let duration = Duration::from_secs(10);
//! safina::timer::sleep_for(duration).await;
//! # }
//! ```
//!
//! ```rust
//! # use core::time::Duration;
//! # use std::time::Instant;
//! # async fn f() {
//! safina::timer::start_timer_thread();
//! let deadline =
//!     Instant::now() + Duration::from_secs(1);
//! safina::timer::sleep_until(deadline).await;
//! # }
//! ```
//!
//! ```rust
//! # use core::time::Duration;
//! # use std::time::Instant;
//! # async fn read_request() -> Result<(), std::io::Error> { Ok(()) }
//! # async fn read_data(id: ()) -> Result<(), std::io::Error> { Ok(()) }
//! # fn process_data(data: ()) -> Result<(), std::io::Error> { Ok(()) }
//! # async fn write_data(data: ()) -> Result<(), std::io::Error> { Ok(()) }
//! # async fn send_response(response: ()) -> Result<(), std::io::Error> { Ok(()) }
//! # async fn f() -> Result<(), std::io::Error> {
//! safina::timer::start_timer_thread();
//! let deadline =
//!     Instant::now() + Duration::from_secs(1);
//! let req = safina::timer::with_deadline(
//!     read_request(), deadline).await??;
//! let data = safina::timer::with_deadline(
//!     read_data(req), deadline).await??;
//! safina::timer::with_deadline(
//!     write_data(data), deadline ).await??;
//! safina::timer::with_deadline(
//!     send_response(data), deadline).await??;
//! # Ok(())
//! # }
//! ```
//!
//! ```rust
//! # use core::time::Duration;
//! # use std::time::Instant;
//! # async fn read_request() -> Result<(), std::io::Error> { Ok(()) }
//! # async fn read_data(id: ()) -> Result<(), std::io::Error> { Ok(()) }
//! # fn process_data(data: ()) -> Result<(), std::io::Error> { Ok(()) }
//! # async fn write_data(data: ()) -> Result<(), std::io::Error> { Ok(()) }
//! # async fn send_response(response: ()) -> Result<(), std::io::Error> { Ok(()) }
//! # async fn f() -> Result<(), std::io::Error> {
//! safina::timer::start_timer_thread();
//! let req = safina::timer::with_timeout(
//!     read_request(), Duration::from_secs(1)
//! ).await??;
//! let data = safina::timer::with_timeout(
//!     read_data(req), Duration::from_secs(2)
//! ).await??;
//! safina::timer::with_timeout(
//!     write_data(data), Duration::from_secs(2)
//! ).await??;
//! safina::timer::with_timeout(
//!     send_response(data),
//!     Duration::from_secs(1)
//! ).await??;
//! # Ok(())
//! # }
//! ```
//!
//! # Alternatives
//! - [futures-timer](https://crates.io/crates/futures-timer)
//!   - popular
//!   - Supports: Wasm, Linux, Windows, macOS
//!   - Contains generous amounts of `unsafe` code
//!   - Uses `std::thread::park_timeout` as its source of time
//! - [async-io](https://crates.io/crates/async-io)
//!   - popular
//!   - single and repeating timers
//!   - Supports: Linux, Windows, macOS, iOS, Android, and many others.
//!   - Uses [polling](https://crates.io/crates/polling) crate
//!     which makes unsafe calls to OS.
//! - [async-timer](https://crates.io/crates/async-timer)
//!   - Supports: Linux & Android
//!   - Makes unsafe calls to OS
//! - [tokio](https://crates.io/crates/tokio)
//!   - very popular
//!   - single and repeating timers
//!   - Supports: Linux, macOS, other unix-like operating systems, Windows
//!   - Fast, internally complicated, and full of `unsafe`
//! - [embedded-async-timer](https://crates.io/crates/embedded-async-timer)
//!   - `no_std`
//!   - Supports `bare_metal`
//!
//! # TO DO
//! - Add a way to schedule jobs (`FnOnce` structs).
mod deadline_future;
pub use deadline_future::*;

mod sleep_future;
pub use sleep_future::*;

use core::cmp::Reverse;
use core::fmt::{Debug, Display, Formatter};
use core::task::Waker;
use std::collections::BinaryHeap;
use std::error::Error;
use std::sync::mpsc::{Receiver, RecvTimeoutError, SyncSender};
use std::sync::{Arc, Mutex, OnceLock};
use std::time::Instant;

/// An internal struct used by [safina::timer].
#[doc(hidden)]
#[derive(Debug)]
pub struct ScheduledWake {
    pub instant: Instant,
    pub waker: Arc<Mutex<Option<Waker>>>,
}
impl ScheduledWake {
    pub fn wake(&self) {
        if let Some(waker) = self
            .waker
            .lock()
            .unwrap_or_else(std::sync::PoisonError::into_inner)
            .take()
        {
            waker.wake();
        }
    }
}
impl PartialEq for ScheduledWake {
    fn eq(&self, other: &Self) -> bool {
        PartialEq::eq(&self.instant, &other.instant)
    }
}
impl Eq for ScheduledWake {}
impl PartialOrd for ScheduledWake {
    fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for ScheduledWake {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.instant.cmp(&other.instant)
    }
}

static TIMER_THREAD_SENDER: OnceLock<SyncSender<ScheduledWake>> = OnceLock::new();

/// Starts the worker thread, if it's not already started.
/// You must call this before calling [`sleep_until`] or [`sleep_for`].
///
/// The thread name is `"safina::timer"`.
#[allow(clippy::missing_panics_doc)]
#[allow(clippy::module_name_repetitions)]
pub fn start_timer_thread() {
    TIMER_THREAD_SENDER.get_or_init(|| {
        let (sender, receiver) = std::sync::mpsc::sync_channel(0);
        std::thread::Builder::new()
            .name("safina::timer".to_string())
            .spawn(|| timer_thread(receiver))
            .unwrap();
        sender
    });
}

/// An internal function used by [crate::timer].
#[doc(hidden)]
#[allow(clippy::module_name_repetitions)]
#[allow(clippy::needless_pass_by_value)]
pub fn timer_thread(receiver: Receiver<ScheduledWake>) {
    let mut heap: BinaryHeap<Reverse<ScheduledWake>> = BinaryHeap::new();
    loop {
        if let Some(Reverse(peeked_wake)) = heap.peek() {
            let now = Instant::now();
            if peeked_wake.instant < now {
                heap.pop().unwrap().0.wake();
            } else {
                // We can switch to recv_deadline once it is stable:
                // https://doc.rust-lang.org/std/sync/mpsc/struct.Receiver.html#method.recv_deadline
                match receiver.recv_timeout(peeked_wake.instant.saturating_duration_since(now)) {
                    Ok(new_wake) => {
                        heap.push(Reverse(new_wake));
                    }
                    Err(RecvTimeoutError::Timeout) => {}
                    Err(RecvTimeoutError::Disconnected) => unreachable!(),
                }
            }
        } else {
            heap.push(Reverse(receiver.recv().unwrap()));
        }
    }
}

/// Call [`start_timer_thread`] to prevent this error.
#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Eq, PartialEq)]
pub struct TimerThreadNotStarted {}
impl Display for TimerThreadNotStarted {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        Debug::fmt(self, f)
    }
}
impl Error for TimerThreadNotStarted {}

fn schedule_wake(
    instant: Instant,
    waker: Arc<Mutex<Option<Waker>>>,
) -> Result<(), TimerThreadNotStarted> {
    let sender = TIMER_THREAD_SENDER.get().ok_or(TimerThreadNotStarted {})?;
    sender.send(ScheduledWake { instant, waker }).unwrap();
    Ok(())
}
