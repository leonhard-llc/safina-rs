use crate::timer::schedule_wake;
use core::fmt::{Debug, Display, Formatter};
use core::future::Future;
use core::pin::Pin;
use core::task::{Context, Poll, Waker};
use std::error::Error;
use std::sync::{Arc, Mutex};
use std::time::Instant;

#[derive(Debug, PartialEq)]
pub enum DeadlineError {
    TimerThreadNotStarted,
    DeadlineExceeded,
}
impl From<DeadlineError> for std::io::Error {
    fn from(error: DeadlineError) -> Self {
        match error {
            DeadlineError::TimerThreadNotStarted => {
                std::io::Error::new(std::io::ErrorKind::Other, "TimerThreadNotStarted")
            }
            DeadlineError::DeadlineExceeded => {
                std::io::Error::new(std::io::ErrorKind::TimedOut, "DeadlineExceeded")
            }
        }
    }
}
impl Display for DeadlineError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        Debug::fmt(self, f)
    }
}
impl Error for DeadlineError {}

#[derive(Debug, PartialEq)]
pub struct DeadlineExceededError;
impl From<DeadlineExceededError> for std::io::Error {
    fn from(_error: DeadlineExceededError) -> Self {
        std::io::Error::new(std::io::ErrorKind::TimedOut, "DeadlineExceededError")
    }
}
impl Display for DeadlineExceededError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        Debug::fmt(self, f)
    }
}
impl Error for DeadlineExceededError {}

/// A future wrapper that returns [`DeadlineExceededError`] at a specified deadline.
///
/// It is returned by [`with_deadline`] and [`with_timeout`].
#[must_use = "futures stay idle unless you await them"]
pub struct DeadlineFuture<Fut: Future + Unpin> {
    inner: Fut,
    deadline: Instant,
    waker: Arc<Mutex<Option<Waker>>>,
}
impl<Fut: Future + Unpin> DeadlineFuture<Fut> {
    // TODO: Remove html links.
    /// Makes a future that awaits `inner`,
    /// but returns [`DeadlineError`] after `deadline`.
    ///
    /// Note that `inner` must be [Unpin].
    /// Use [`Box::pin`] to make it Unpin.
    /// Or use [`pin_utils::pin_mut`](https://docs.rs/pin-utils/latest/pin_utils/macro.pin_mut.html)
    /// to do it with unsafe code that does not allocate memory.
    pub fn new(inner: Fut, deadline: Instant) -> Self {
        Self {
            inner,
            deadline,
            waker: Arc::new(Mutex::new(None)),
        }
    }
}
impl<Fut: Future + Unpin> Future for DeadlineFuture<Fut> {
    type Output = Result<Fut::Output, DeadlineError>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        // The primary purpose of deadlines is to shed load during overload.
        // If the inner future completed and the deadline exceeded, the process
        // is likely overloaded.  In this case, we return error to shed load.
        if self.deadline < Instant::now() {
            return Poll::Ready(Err(DeadlineError::DeadlineExceeded));
        }
        match Pin::new(&mut self.inner).poll(cx) {
            Poll::Ready(r) => return Poll::Ready(Ok(r)),
            Poll::Pending => {}
        }
        let old_waker = self.waker.lock().unwrap().replace(cx.waker().clone());
        if old_waker.is_none() {
            schedule_wake(self.deadline, self.waker.clone())
                .map_err(|_| DeadlineError::TimerThreadNotStarted)?;
        }
        Poll::Pending
    }
}

/// Awaits `inner`, but returns [`DeadlineExceededError`] after `deadline`.
///
/// First moves `inner` to the heap, to make it [Unpin].
/// Use [`DeadlineFuture::new`] to avoid allocating on the heap.
///
/// # Errors
/// Returns [`DeadlineExceededError`] if `deadline` passes before `inner` completes.
///
/// # Panics
/// Panics if [`crate::timer::start_timer_thread`] has not been called.
/// If you need to handle this error, use [`DeadlineFuture::new`].
pub async fn with_deadline<Fut: Future>(
    inner: Fut,
    deadline: Instant,
) -> Result<Fut::Output, DeadlineExceededError> {
    match DeadlineFuture::new(Box::pin(inner), deadline).await {
        Ok(result) => Ok(result),
        Err(DeadlineError::DeadlineExceeded) => Err(DeadlineExceededError),
        Err(DeadlineError::TimerThreadNotStarted) => panic!("TimerThreadNotStarted"),
    }
}

/// Awaits `inner`, but returns [`DeadlineExceededError`] after `duration` time from now.
///
/// First moves `inner` to the heap, to make it [Unpin].
/// Use [`DeadlineFuture::new`] to avoid allocating on the heap.
///
/// # Errors
/// Returns [`DeadlineExceededError`] if `duration` passes before `inner` completes.
///
/// # Panics
/// Panics if [`crate::timer::start_timer_thread`] has not been called.
/// If you need to handle this error, use [`DeadlineFuture::new`].
pub async fn with_timeout<Fut: Future>(
    inner: Fut,
    duration: std::time::Duration,
) -> Result<Fut::Output, DeadlineExceededError> {
    with_deadline(inner, Instant::now() + duration).await
}
