use crate::timer::{schedule_wake, TimerThreadNotStarted};
use core::future::Future;
use core::pin::Pin;
use core::task::{Context, Poll, Waker};
use std::sync::{Arc, Mutex};
use std::time::Instant;

/// A future that completes after the specified time.
///
/// It is returned by [`sleep_until`] and [`sleep_for`].
#[must_use = "futures stay idle unless you await them"]
pub struct SleepFuture {
    deadline: Instant,
    waker: Arc<Mutex<Option<Waker>>>,
}
impl SleepFuture {
    pub fn new(deadline: Instant) -> Self {
        Self {
            deadline,
            waker: Arc::new(Mutex::new(None)),
        }
    }
}
impl Future for SleepFuture {
    type Output = Result<(), TimerThreadNotStarted>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.deadline < Instant::now() {
            return Poll::Ready(Ok(()));
        }
        let old_waker = self.waker.lock().unwrap().replace(cx.waker().clone());
        if old_waker.is_none() {
            schedule_wake(self.deadline, self.waker.clone())?;
        }
        Poll::Pending
    }
}

/// Returns after `deadline`.
///
/// # Panics
/// Panics if [`crate::timer::start_timer_thread`] has not been called.
/// If you need to handle this error, use [`SleepFuture::new`].
pub async fn sleep_until(deadline: Instant) {
    SleepFuture::new(deadline).await.unwrap();
}

/// Returns `duration` time from now.
///
/// # Panics
/// Panics if [`crate::timer::start_timer_thread`] has not been called.
/// If you need to handle this error, use [`SleepFuture::new`].
pub async fn sleep_for(duration: core::time::Duration) {
    SleepFuture::new(Instant::now() + duration).await.unwrap();
}
