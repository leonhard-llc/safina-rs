use safina::select::OptionAbc;

#[allow(clippy::type_complexity)]
fn test_values() -> (
    OptionAbc<bool, u8, &'static str>,
    OptionAbc<bool, u8, &'static str>,
    OptionAbc<bool, u8, &'static str>,
) {
    (OptionAbc::A(true), OptionAbc::B(42), OptionAbc::C("s1"))
}

#[test]
fn test_as_ref() {
    let (a, b, c) = test_values();
    let _: OptionAbc<&bool, &u8, &&'static str> = a.as_ref();
    let _: &u8 = b.as_ref().unwrap_b();
    let _: &&'static str = c.as_ref().unwrap_c();
    assert!(*(a.as_ref().unwrap_a()));
    assert_eq!(42_u8, *(b.as_ref().unwrap_b()));
    assert_eq!("s1", *(c.as_ref().unwrap_c()));
}

#[test]
fn test_accessors() {
    let (a, b, c) = test_values();
    assert!(*(a.a().unwrap()));
    assert_eq!(None, a.b());
    assert_eq!(None, a.c());

    assert_eq!(None, b.a());
    assert_eq!(42_u8, *(b.b().unwrap()));
    assert_eq!(None, b.c());

    assert_eq!(None, c.a());
    assert_eq!(None, c.b());
    assert_eq!("s1", *(c.c().unwrap()));
}

#[test]
fn test_debug() {
    let (a, b, c) = test_values();
    assert_eq!("OptionABC::A(true)", format!("{a:?}"));
    assert_eq!("OptionABC::B(42)", format!("{b:?}"));
    assert_eq!("OptionABC::C(\"s1\")", format!("{c:?}"));
}

#[test]
fn test_display() {
    let (a, b, c) = test_values();
    assert_eq!("true", format!("{a}"));
    assert_eq!("42", format!("{b}"));
    assert_eq!("s1", format!("{c}"));
}

#[test]
fn test_eq() {
    let (a, b, c) = test_values();
    assert_eq!(OptionAbc::A(true), a);
    assert_ne!(OptionAbc::A(false), a);
    assert_eq!(OptionAbc::B(42_u8), b);
    assert_ne!(OptionAbc::B(2_u8), b);
    assert_eq!(OptionAbc::C("s1"), c);
    assert_ne!(OptionAbc::C("other"), c);
    assert_ne!(a, b);
    assert_ne!(a, c);
    assert_ne!(b, c);
}

#[test]
fn test_unwrap() {
    let (a, b, c) = test_values();
    let a_clone = a.clone();
    assert!(a_clone.unwrap_a());
    let a_clone = a.clone();
    assert_eq!(
        "expected OptionABC::B(_) but found OptionABC::A(true)",
        std::panic::catch_unwind(|| a_clone.unwrap_b())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );
    let a_clone = a.clone();
    assert_eq!(
        "expected OptionABC::C(_) but found OptionABC::A(true)",
        std::panic::catch_unwind(|| a_clone.unwrap_c())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );

    let b_clone = b.clone();
    assert_eq!(
        "expected OptionABC::A(_) but found OptionABC::B(42)",
        std::panic::catch_unwind(|| b_clone.unwrap_a())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );
    let b_clone = b.clone();
    assert_eq!(42_u8, b_clone.unwrap_b());
    let b_clone = b.clone();
    std::panic::catch_unwind(|| b_clone.unwrap_c()).unwrap_err();

    let c_clone = c.clone();
    std::panic::catch_unwind(|| c_clone.unwrap_a()).unwrap_err();
    let c_clone = c.clone();
    std::panic::catch_unwind(|| c_clone.unwrap_b()).unwrap_err();
    let c_clone = c.clone();
    assert_eq!("s1", c_clone.unwrap_c());
}

#[test]
fn test_take() {
    let same_a: OptionAbc<u8, u8, u8> = OptionAbc::A(42_u8);
    let same_b: OptionAbc<u8, u8, u8> = OptionAbc::B(42_u8);
    let same_c: OptionAbc<u8, u8, u8> = OptionAbc::C(42_u8);
    assert_eq!(42_u8, same_a.take());
    assert_eq!(42_u8, same_b.take());
    assert_eq!(42_u8, same_c.take());
}
