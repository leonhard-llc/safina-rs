use safina::async_test;
use std::sync::atomic::{AtomicBool, Ordering};
use std::time::{Duration, Instant};

static TEST_RAN: AtomicBool = AtomicBool::new(false);

#[async_test]
#[allow(clippy::unused_async)]
async fn run_test() {
    TEST_RAN.store(true, Ordering::Release);
}

#[test]
fn should_run_test() {
    let deadline = Instant::now() + Duration::from_millis(1000);
    while Instant::now() < deadline {
        if TEST_RAN.load(Ordering::Acquire) {
            return;
        }
        std::hint::spin_loop();
    }
    panic!("timeout");
}

#[async_test]
#[allow(clippy::unused_async)]
async fn should_work_with_spawned_tasks() {
    let (sender, receiver) = std::sync::mpsc::channel();
    safina::executor::spawn(async move {
        safina::executor::spawn(async move {
            sender.send(()).unwrap();
        });
    });
    receiver.recv().unwrap();
}

#[async_test]
async fn should_start_timer_thread() {
    safina::timer::sleep_for(Duration::from_millis(10)).await;
}

#[async_test]
async fn under_default_timeout() {
    safina::timer::sleep_for(Duration::from_millis(4900)).await;
}

#[async_test]
#[should_panic(expected = "timed out")]
async fn over_default_timeout() {
    safina::timer::sleep_for(Duration::from_millis(5100)).await;
}

#[async_test(timeout_sec = 1)]
async fn under_custom_timeout() {
    safina::timer::sleep_for(Duration::from_millis(900)).await;
}

#[async_test(timeout_sec = 1)]
#[should_panic(expected = "timed out")]
async fn over_custom_timeout() {
    safina::timer::sleep_for(Duration::from_millis(1100)).await;
}

// // error[E0308]: mismatched types; expected `()`, found enum `std::result::Result`
// #[async_test]
// async fn dont_allow_returning_result() -> Result<(), std::sync::mpsc::RecvError> {
//     Ok(())
// }
