#![allow(clippy::many_single_char_names)]
#![allow(clippy::float_cmp)]
use safina::select::OptionAbcde;

#[allow(clippy::type_complexity)]
fn test_values() -> (
    OptionAbcde<bool, u8, &'static str, usize, f32>,
    OptionAbcde<bool, u8, &'static str, usize, f32>,
    OptionAbcde<bool, u8, &'static str, usize, f32>,
    OptionAbcde<bool, u8, &'static str, usize, f32>,
    OptionAbcde<bool, u8, &'static str, usize, f32>,
) {
    (
        OptionAbcde::A(true),
        OptionAbcde::B(42),
        OptionAbcde::C("s1"),
        OptionAbcde::D(7_usize),
        OptionAbcde::E(0.99_f32),
    )
}

#[test]
fn test_as_ref() {
    let (a, b, c, d, e) = test_values();
    let _: OptionAbcde<&bool, &u8, &&'static str, &usize, &f32> = a.as_ref();
    let _: &u8 = b.as_ref().unwrap_b();
    let _: &&'static str = c.as_ref().unwrap_c();
    let _: &usize = d.as_ref().unwrap_d();
    let _: &f32 = e.as_ref().unwrap_e();
    assert!(*(a.as_ref().unwrap_a()));
    assert_eq!(42_u8, *(b.as_ref().unwrap_b()));
    assert_eq!("s1", *(c.as_ref().unwrap_c()));
    assert_eq!(7_usize, *(d.as_ref().unwrap_d()));
    assert_eq!(0.99_f32, *(e.as_ref().unwrap_e()));
}

#[test]
fn test_accessors() {
    let (a, b, c, d, e) = test_values();
    // Test accessors
    assert!(*(a.a().unwrap()));
    assert_eq!(None, a.b());
    assert_eq!(None, a.c());
    assert_eq!(None, a.d());
    assert_eq!(None, a.e());

    assert_eq!(None, b.a());
    assert_eq!(42_u8, *(b.b().unwrap()));
    assert_eq!(None, b.c());
    assert_eq!(None, b.d());
    assert_eq!(None, b.e());

    assert_eq!(None, c.a());
    assert_eq!(None, c.b());
    assert_eq!("s1", *(c.c().unwrap()));
    assert_eq!(None, c.d());
    assert_eq!(None, c.e());

    assert_eq!(None, d.a());
    assert_eq!(None, d.b());
    assert_eq!(None, d.c());
    assert_eq!(7_usize, *(d.d().unwrap()));
    assert_eq!(None, d.e());

    assert_eq!(None, e.a());
    assert_eq!(None, e.a());
    assert_eq!(None, e.a());
    assert_eq!(None, e.a());
    assert_eq!(0.99_f32, *(e.e().unwrap()));
}

#[test]
fn test_debug() {
    let (a, b, c, d, e) = test_values();
    assert_eq!("OptionABCDE::A(true)", format!("{a:?}"));
    assert_eq!("OptionABCDE::B(42)", format!("{b:?}"));
    assert_eq!("OptionABCDE::C(\"s1\")", format!("{c:?}"));
    assert_eq!("OptionABCDE::D(7)", format!("{d:?}"));
    assert_eq!("OptionABCDE::E(0.99)", format!("{e:?}"));
}

#[test]
fn test_display() {
    let (a, b, c, d, e) = test_values();
    assert_eq!("true", format!("{a}"));
    assert_eq!("42", format!("{b}"));
    assert_eq!("s1", format!("{c}"));
    assert_eq!("7", format!("{d}"));
    assert_eq!("0.99", format!("{e}"));
}

#[test]
fn test_eq() {
    let (a, b, c, d, e) = test_values();
    assert_eq!(OptionAbcde::A(true), a);
    assert_ne!(OptionAbcde::A(false), a);
    assert_eq!(OptionAbcde::B(42_u8), b);
    assert_ne!(OptionAbcde::B(2_u8), b);
    assert_eq!(OptionAbcde::C("s1"), c);
    assert_ne!(OptionAbcde::C("other"), c);
    assert_eq!(OptionAbcde::D(7_usize), d);
    assert_ne!(OptionAbcde::D(2_usize), d);
    assert_eq!(OptionAbcde::E(0.99_f32), e);
    assert_ne!(OptionAbcde::E(0.1_f32), e);
    assert_ne!(a, b);
    assert_ne!(a, c);
    assert_ne!(a, d);
    assert_ne!(a, e);
    assert_ne!(b, c);
    assert_ne!(b, d);
    assert_ne!(b, e);
    assert_ne!(c, d);
    assert_ne!(c, e);
    assert_ne!(d, e);
}

#[test]
fn test_unwrap() {
    let (a, b, c, d, e) = test_values();
    let a_clone = a.clone();
    assert!(a_clone.unwrap_a());
    let a_clone = a.clone();
    assert_eq!(
        "expected OptionABCDE::B(_) but found OptionABCDE::A(true)",
        std::panic::catch_unwind(|| a_clone.unwrap_b())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );
    let a_clone = a.clone();
    assert_eq!(
        "expected OptionABCDE::C(_) but found OptionABCDE::A(true)",
        std::panic::catch_unwind(|| a_clone.unwrap_c())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );
    let a_clone = a.clone();
    assert_eq!(
        "expected OptionABCDE::D(_) but found OptionABCDE::A(true)",
        std::panic::catch_unwind(|| a_clone.unwrap_d())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );
    let a_clone = a.clone();
    assert_eq!(
        "expected OptionABCDE::E(_) but found OptionABCDE::A(true)",
        std::panic::catch_unwind(|| a_clone.unwrap_e())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );

    let b_clone = b.clone();
    assert_eq!(
        "expected OptionABCDE::A(_) but found OptionABCDE::B(42)",
        std::panic::catch_unwind(|| b_clone.unwrap_a())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );
    let b_clone = b.clone();
    assert_eq!(42_u8, b_clone.unwrap_b());
    let b_clone = b.clone();
    std::panic::catch_unwind(|| b_clone.unwrap_c()).unwrap_err();
    let b_clone = b.clone();
    std::panic::catch_unwind(|| b_clone.unwrap_d()).unwrap_err();
    let b_clone = b.clone();
    std::panic::catch_unwind(|| b_clone.unwrap_e()).unwrap_err();

    let c_clone = c.clone();
    std::panic::catch_unwind(|| c_clone.unwrap_a()).unwrap_err();
    let c_clone = c.clone();
    std::panic::catch_unwind(|| c_clone.unwrap_b()).unwrap_err();
    let c_clone = c.clone();
    assert_eq!("s1", c_clone.unwrap_c());
    let c_clone = c.clone();
    std::panic::catch_unwind(|| c_clone.unwrap_d()).unwrap_err();
    let c_clone = c.clone();
    std::panic::catch_unwind(|| c_clone.unwrap_e()).unwrap_err();

    let d_clone = d.clone();
    std::panic::catch_unwind(|| d_clone.unwrap_a()).unwrap_err();
    let d_clone = d.clone();
    std::panic::catch_unwind(|| d_clone.unwrap_b()).unwrap_err();
    let d_clone = d.clone();
    std::panic::catch_unwind(|| d_clone.unwrap_c()).unwrap_err();
    let d_clone = d.clone();
    assert_eq!(7_usize, d_clone.unwrap_d());
    let d_clone = d.clone();
    std::panic::catch_unwind(|| d_clone.unwrap_e()).unwrap_err();

    let e_clone = e.clone();
    std::panic::catch_unwind(|| e_clone.unwrap_a()).unwrap_err();
    let e_clone = e.clone();
    std::panic::catch_unwind(|| e_clone.unwrap_b()).unwrap_err();
    let e_clone = e.clone();
    std::panic::catch_unwind(|| e_clone.unwrap_c()).unwrap_err();
    let e_clone = e.clone();
    std::panic::catch_unwind(|| e_clone.unwrap_d()).unwrap_err();
    let e_clone = e.clone();
    assert_eq!(0.99_f32, e_clone.unwrap_e());
}

#[test]
fn test_take() {
    let same_a: OptionAbcde<u8, u8, u8, u8, u8> = OptionAbcde::A(42_u8);
    let same_b: OptionAbcde<u8, u8, u8, u8, u8> = OptionAbcde::B(42_u8);
    let same_c: OptionAbcde<u8, u8, u8, u8, u8> = OptionAbcde::C(42_u8);
    let same_d: OptionAbcde<u8, u8, u8, u8, u8> = OptionAbcde::C(42_u8);
    let same_e: OptionAbcde<u8, u8, u8, u8, u8> = OptionAbcde::C(42_u8);
    assert_eq!(42_u8, same_a.take());
    assert_eq!(42_u8, same_b.take());
    assert_eq!(42_u8, same_c.take());
    assert_eq!(42_u8, same_d.take());
    assert_eq!(42_u8, same_e.take());
}
