use safina::select::OptionAb;

fn test_values() -> (OptionAb<bool, u8>, OptionAb<bool, u8>) {
    (OptionAb::A(true), OptionAb::B(42))
}

#[test]
fn test_as_ref() {
    let (a, b) = test_values();
    let _: OptionAb<&bool, &u8> = a.as_ref();
    let _: &u8 = b.as_ref().unwrap_b();
    assert!(*(a.as_ref().unwrap_a()));
    assert_eq!(42_u8, *(b.as_ref().unwrap_b()));
}

#[test]
fn test_accessors() {
    let (a, b) = test_values();
    assert!(*(a.a().unwrap()));
    assert_eq!(None, a.b());
    assert_eq!(None, b.a());
    assert_eq!(42_u8, *(b.b().unwrap()));
}

#[test]
fn test_debug() {
    let (a, b) = test_values();
    assert_eq!("OptionAB::A(true)", format!("{a:?}"));
    assert_eq!("OptionAB::B(42)", format!("{b:?}"));
}

#[test]
fn test_display() {
    let (a, b) = test_values();
    assert_eq!("true", format!("{a}"));
    assert_eq!("42", format!("{b}"));
}

#[test]
fn test_eq() {
    let (a, b) = test_values();
    assert_eq!(OptionAb::A(true), a);
    assert_ne!(OptionAb::A(false), a);
    assert_eq!(OptionAb::B(42_u8), b);
    assert_ne!(OptionAb::B(2_u8), b);
    assert_ne!(a, b);
}

#[allow(clippy::redundant_clone)]
#[test]
fn test_unwrap() {
    let (a, b) = test_values();
    let a_clone = a.clone();
    assert!(a_clone.unwrap_a());
    let a_clone = a.clone();
    assert_eq!(
        "expected OptionAB::B(_) but found OptionAB::A(true)",
        std::panic::catch_unwind(|| a_clone.unwrap_b())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );

    let b_clone = b.clone();
    assert_eq!(
        "expected OptionAB::A(_) but found OptionAB::B(42)",
        std::panic::catch_unwind(|| b_clone.unwrap_a())
            .unwrap_err()
            .downcast::<String>()
            .unwrap()
            .as_str()
    );
    let b_clone = b.clone();
    assert_eq!(42_u8, b_clone.unwrap_b());
}

#[test]
fn test_take() {
    let same_a: OptionAb<u8, u8> = OptionAb::A(42_u8);
    let same_b: OptionAb<u8, u8> = OptionAb::B(42_u8);
    assert_eq!(42_u8, same_a.take());
    assert_eq!(42_u8, same_b.take());
}
