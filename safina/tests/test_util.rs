#![allow(dead_code)]
use std::ops::Range;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;
use std::task::Waker;
use std::time::{Duration, Instant};

#[derive(Clone)]
pub struct FakeWaker {
    called_flag: Arc<AtomicBool>,
}
impl FakeWaker {
    #[must_use]
    pub fn new(called_flag: &Arc<AtomicBool>) -> Self {
        Self {
            called_flag: called_flag.clone(),
        }
    }
    #[must_use]
    pub fn into_waker(self) -> Waker {
        std::task::Waker::from(Arc::new(self))
    }
}
impl std::task::Wake for FakeWaker {
    fn wake(self: Arc<Self>) {
        if self
            .called_flag
            .fetch_or(true, std::sync::atomic::Ordering::AcqRel)
        {
            panic!("wake already called");
        }
    }
}

/// # Panics
/// Panics if the time elapsed since `before` is outside of `range_ms`.
pub fn expect_elapsed(before: Instant, range_ms: Range<u64>) {
    assert!(!range_ms.is_empty(), "invalid range {:?}", range_ms);
    let elapsed = before.elapsed();
    let duration_range = Duration::from_millis(range_ms.start)..Duration::from_millis(range_ms.end);
    assert!(
        duration_range.contains(&elapsed),
        "{:?} elapsed, out of range {:?}",
        elapsed,
        duration_range
    );
}
