use safina::executor::Executor;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::sync::Arc;

#[test]
fn main() {
    // Start a simple server that a thread and std::net blocking sockets.
    let bind_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::LOCALHOST), 0);
    let listener = std::net::TcpListener::bind(bind_addr).unwrap();
    let listen_addr = listener.local_addr().unwrap();
    println!("SERVER listening on {}", &listen_addr);
    std::thread::spawn(move || {
        let (mut tcp_stream, remote_addr) = listener.accept().unwrap();
        println!("SERVER accepted connection {remote_addr}");
        let mut request = String::new();
        std::io::Read::read_to_string(&mut tcp_stream, &mut request).unwrap();
        println!("SERVER received request {request:?}");
        let response = "response1";
        println!("SERVER sending response {response:?}");
        std::io::Write::write_all(&mut tcp_stream, response.as_bytes()).unwrap();
        tcp_stream.shutdown(std::net::Shutdown::Write).unwrap();
    });

    // Start an async task that uses safina::net to connect to the server and
    // send & receive data.
    safina::timer::start_timer_thread();
    let executor: Arc<Executor> = Arc::default();
    executor.block_on(async move {
        println!("CLIENT connecting");
        let mut tcp_stream = safina::net::TcpStream::connect(listen_addr).await.unwrap();
        let request = "request1";
        println!("CLIENT sending request {request:?}");
        tcp_stream.write_all(request.as_bytes()).await.unwrap();
        tcp_stream
            .inner()
            .shutdown(std::net::Shutdown::Write)
            .unwrap();
        println!("CLIENT reading response");
        let mut response = String::new();
        tcp_stream.read_to_string(&mut response).await.unwrap();
        println!("CLIENT received response {response:?}");
        assert_eq!("response1", response);
    });
}
