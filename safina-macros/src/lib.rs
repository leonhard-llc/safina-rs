//! # safina-macros
//! [![crates.io version](https://img.shields.io/crates/v/safina-async-test-core.svg)](https://crates.io/crates/safina-macros)
//! [![license: Apache 2.0](https://gitlab.com/leonhard-llc/safina-rs/-/raw/main/license-apache-2.0.svg)](http://www.apache.org/licenses/LICENSE-2.0)
//! [![unsafe forbidden](https://gitlab.com/leonhard-llc/safina-rs/-/raw/main/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)
//! [![pipeline status](https://gitlab.com/leonhard-llc/safina-rs/badges/main/pipeline.svg)](https://gitlab.com/leonhard-llc/safina-rs/-/pipelines)
//!
//! Procedural macros for the [safina](https://crates.io/crates/safina) crate.
//!
//! License: Apache-2.0
//!
//! # Cargo Geiger Safety Report
#![forbid(unsafe_code)]

mod async_test;

/// A macro for running `async fn` tests.
///
/// - Runs tests with [safina::executor](https://docs.rs/safina/latest/safina/executor/)
/// - Each test gets its own executor with 2 threads for async tasks and 1 thread for blocking tasks.
/// - Also calls
///   [safina::timer::start_timer_thread](https://docs.rs/safina/latest/safina/timer/fn.start_timer_thread.html)
///   before running the test
/// - Lightweight dependencies
///
/// # Examples
/// ```rust
/// use safina::async_test;
/// # async fn async_work() -> Result<(), std::io::Error> { Ok(()) }
///
/// #[async_test]
/// async fn test1() {
///     async_work().await.unwrap();
/// }
/// ```
///
/// ```rust
/// use safina::async_test;
/// # use core::time::Duration;
/// # fn blocking_work() -> Result<u8, std::io::Error> { Ok(3) }
/// # async fn background_task() {}
/// # async fn async_work() -> Result<u8, std::io::Error> { Ok(42) }
///
/// // Make your test an `async fn`.
/// #[async_test]
/// async fn test2() {
///     // You can `await`.
///     async_work().await.unwrap();
///
///     // You can spawn tasks which will run on
///     // the executor.
///     // These tasks stop when the test
///     // function returns and drops the
///     // executor.
///     safina::executor::spawn(background_task());
///
///     // You can run blocking code without
///     // stalling other async tasks.
///     let result = safina::executor::schedule_blocking(
///         || blocking_work()
///     ).async_recv().await.unwrap();
///     assert_eq!(3, result.unwrap());
///
///     // You can use timer functions.
///     safina::timer::sleep_for(
///         Duration::from_millis(10)).await;
///     safina::timer::with_timeout(
///         async_work(),
///         Duration::from_millis(100)
///     ).await.unwrap().unwrap();
/// }
/// ```
///
/// # Alternatives
/// - [async_std::test](https://docs.rs/async-std/latest/async_std/attr.test.html)
/// - [futures_await_test::async_test](https://docs.rs/futures-await-test)
/// - [tokio::test](https://docs.rs/tokio/latest/tokio/attr.test.html)
#[proc_macro_attribute]
pub fn async_test(
    attr: proc_macro::TokenStream,
    item: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let output2 = match async_test::implementation(
        safe_proc_macro2::TokenStream::from(attr),
        safe_proc_macro2::TokenStream::from(item),
    ) {
        Ok(output) => output,
        Err(error_tokens) => return proc_macro::TokenStream::from(error_tokens),
    };

    // let mut token_stream_string = String::new();
    // for tree in output {
    //     token_stream_string.push_str(format!("tree: {tree}\n").as_str());
    // }
    // panic!("TokenStream:\n{}", token_stream_string);

    proc_macro::TokenStream::from(output2)
}
