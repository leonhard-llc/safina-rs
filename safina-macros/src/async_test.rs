use safe_proc_macro2::{TokenStream, TokenTree};
use safe_quote::{quote_spanned, TokenStreamExt};

pub fn parse_timeout(attr: TokenStream) -> Result<u64, TokenStream> {
    fn err(tree: &TokenTree) -> Result<u64, TokenStream> {
        Err(quote_spanned!(tree.span()=>
            compile_error!("expected attribute like 'timeout_sec=10'");
        ))
    }
    let mut attr_tokens = attr.into_iter();
    let Some(ident_tree) = attr_tokens.next() else {
        return Ok(5);
    };
    let TokenTree::Ident(ref ident) = ident_tree else {
        return err(&ident_tree);
    };
    if *ident != "timeout_sec" {
        return err(&ident_tree);
    }
    let Some(punct_tree) = attr_tokens.next() else {
        return err(&ident_tree);
    };
    let TokenTree::Punct(ref punct) = punct_tree else {
        return err(&punct_tree);
    };
    if punct.as_char() != '=' {
        return err(&punct_tree);
    }
    let Some(literal_tree) = attr_tokens.next() else {
        return err(&punct_tree);
    };
    let TokenTree::Literal(ref literal) = literal_tree else {
        return err(&literal_tree);
    };
    match literal.to_string().parse::<u64>() {
        Ok(timeout) if timeout > 0 => Ok(timeout),
        Ok(_) => Err(quote_spanned!(literal_tree.span()=>
            compile_error!("timeout must be in 1-31_536_000");
        )),
        Err(_) => err(&literal_tree),
    }
}

#[allow(clippy::similar_names)]
pub fn implementation(attr: TokenStream, item: TokenStream) -> Result<TokenStream, TokenStream> {
    let timeout_sec = parse_timeout(attr)?;
    // Ident { ident: "async", span: #0 bytes(50..55) }
    // Ident { ident: "fn", span: #0 bytes(56..58) }
    // Ident { ident: "should_run_async_fn", span: #0 bytes(59..78) }
    // Group { delimiter: Parenthesis, stream: TokenStream [], span: ...
    // Group { delimiter: Brace, stream: TokenStream [Ident { ident: "println",...
    let mut tokens = item.into_iter();
    let mut preamble = TokenStream::new();
    let mut token0: Option<TokenTree> = None;
    let mut token1: Option<TokenTree> = None;
    let mut token2: Option<TokenTree> = None;
    loop {
        if let Some(tree) = token0.take() {
            preamble.append(tree);
        }
        token0 = token1.take();
        token1 = token2.take();
        token2 = tokens.next();
        match (&token0, &token1, &token2) {
            (None, None, None) => panic!("expected async fn"),
            (Some(tree), _, None) | (None, Some(tree), None) => {
                return Err(quote_spanned!(tree.span()=>
                    compile_error!("expected async fn");
                ))
            }
            (
                Some(TokenTree::Ident(ref ident_async)),
                Some(TokenTree::Ident(..)),
                Some(TokenTree::Ident(..)),
            ) if *ident_async == "async" => {
                break;
            }
            _ => continue,
        }
    }
    let ident_async = token0.unwrap();
    let ident_fn = token1.unwrap();
    let ident_name = token2.unwrap();
    let err = format!("async_test '{ident_name}' timed out after {timeout_sec} seconds");
    let remainder = tokens.collect::<TokenStream>();
    Ok(quote_spanned!(ident_name.span()=>
        #[test]
        #preamble
        fn #ident_name () {
            #ident_async #ident_fn #ident_name #remainder
            safina::timer::start_timer_thread();
            let executor = safina::executor::Executor::builder()
                .with_async_threads(2)
                .with_blocking_threads(1)
                .with_thread_name_prefix("test_")
                .build()
                .unwrap();
            let result = executor.block_on(safina::timer::with_timeout(
                #ident_name (),
                Duration::from_secs(#timeout_sec)),
            );
            if result.is_err() {
                panic!(#err);
            }
        }
    ))
}

// The version below has two drawbacks:
// 1. IDEs don't know that the stuff inside `async_test! { async fn test1() { ... } }` is a
//    function, so they don't provide formatting, auto-complete, or other useful editor functions.
//    This is a fatal flaw.
// 2. It puts the test in a module with the same name as the test function,
//    and a test function named `async_test`.
//    So for `async_test! { async fn test1() {...} }` below, it makes
//    `mod test1 { #[test] fn async_test() {...} }`.
//    This makes IDE test output more verbose.  Each test appears as its own category.
//
// macro_rules! async_test {
//     (async fn $name:ident () $body:block) => {
//         async fn $name() $body
//
//         mod $name {
//             #[test]
//             fn async_test () {
//                 safina::executor::block_on(super::$name())
//             }
//         }
//     }
// }
//
// async_test! {
// async fn test1() {
//     println!("test1");
// }
// }

// The version below requires the user to keep the two names in sync.
// Also, the error messages are not great.
//
// macro_rules! async_test {
//     ($name:ident) => {
//         mod $name {
//             #[test]
//             fn async_test () {
//                 safina::executor::block_on(super::$name())
//             }
//         }
//     }
// }
//
// async_test!(test1);
// async fn test1() {
//     println!("test1");
// }
//
// async_test!(test1); // error[E0428]: the name `test1` is defined multiple times
// async fn test2() {
//     println!("test2");
// }
//
// async_test!(test3); // error[E0423]: expected function, found module `super::test3`
// async fn test4() {
//     println!("test4");
// }
