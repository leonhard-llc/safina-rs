# safina-macros
[![crates.io version](https://img.shields.io/crates/v/safina-async-test-core.svg)](https://crates.io/crates/safina-macros)
[![license: Apache 2.0](https://gitlab.com/leonhard-llc/safina-rs/-/raw/main/license-apache-2.0.svg)](http://www.apache.org/licenses/LICENSE-2.0)
[![unsafe forbidden](https://gitlab.com/leonhard-llc/safina-rs/-/raw/main/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)
[![pipeline status](https://gitlab.com/leonhard-llc/safina-rs/badges/main/pipeline.svg)](https://gitlab.com/leonhard-llc/safina-rs/-/pipelines)

Procedural macros for the [safina](https://crates.io/crates/safina) crate.

License: Apache-2.0

# Cargo Geiger Safety Report
```

Metric output format: x/y
    x = unsafe code used by the build
    y = total unsafe code found in the crate

Symbols: 
    🔒  = No `unsafe` usage found, declares #![forbid(unsafe_code)]
    ❓  = No `unsafe` usage found, missing #![forbid(unsafe_code)]
    ☢️  = `unsafe` usage found

Functions  Expressions  Impls  Traits  Methods  Dependency

0/0        0/0          0/0    0/0     0/0      🔒  safina-macros 0.1.3
0/0        0/0          0/0    0/0     0/0      🔒  ├── safe-proc-macro2 1.0.67
0/0        4/4          0/0    0/0     0/0      ☢️  │   └── unicode-ident 1.0.13
0/0        0/0          0/0    0/0     0/0      🔒  └── safe-quote 1.0.15
0/0        0/0          0/0    0/0     0/0      🔒      └── safe-proc-macro2 1.0.67

0/0        4/4          0/0    0/0     0/0    

```
